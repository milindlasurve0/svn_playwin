/*
 *game play script for Playwin android app
 */

var Playwin = (typeof Playwin !== undefined) ? Playwin : {};

$(function() {
	
    ko.extenders.required = function(target, overrideMessage) {
        //add some sub-observables to our observable
        target.hasError = ko.observable();
        target.validationMessage = ko.observable();
	 
        //define a function to do validation
        function validate(newValue) {
            target.hasError(newValue ? false : true);
            target.validationMessage(newValue ? "" : overrideMessage || "This field is required");
        }
	 
        //initial validation
        validate(target());
	 
        //validate whenever the value changes
        target.subscribe(validate);
	 
        //return the original observable
        return target;
    };
    ko.extenders.mobile = function(target, overrideMessage) {
        //add some sub-observables to our observable
        target.hasError = ko.observable();
        target.validationMessage = ko.observable();
	 
        //define a function to do validation
        function validate(newValue) {
            var msg = overrideMessage;
            var status=false;
            if(isNaN(newValue)||newValue.indexOf(" ")!=-1){
                msg = "Your mobile number should contain numeric values";
                status= true;
            }else if (newValue.length != 10){
                msg = "Your mobile number should be a 10 digit number";
                status= true;
            }
	 	  
            target.hasError(status);
            target.validationMessage(status ?  msg : "");
        }
	 
        //initial validation
        validate(target());
        //alert(target.validationMessage());
        //validate whenever the value changes
        target.subscribe(validate);
	 
        //return the original observable
        return target;
    };
    ko.extenders.numeric = function(target, precision) {
        //create a writeable computed observable to intercept writes to our observable
        var result = ko.computed({
            read: target,  //always return the original observables value
            write: function(newValue) {
                var current = target(),
                roundingMultiplier = Math.pow(10, precision),
                newValueAsNum = isNaN(newValue) ? 0 : parseFloat(+newValue),
                valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;
	 
                //only write if it changed
                if (valueToWrite !== current) {
                    target(valueToWrite);
                } else {
                    //if the rounded value is the same, but a different value was written, force a notification for the current field
                    if (newValue !== current) {
                        target.notifySubscribers(valueToWrite);
                    }
                }
            }
        });
	 
        //initialize with current value to make sure it is rounded appropriately
        result(target());
	 
        //return the new computed observable
        return result;
    };
    AppNamespace.registerUser = function () {
        var registerViewModel = {
            message: ko.observable('Playwin User Registration !'),
            name:ko.observable("").extend({
                required: "Please enter a your name."
            }),
            mobile:ko.observable("").extend({
                mobile:"Please enter a correct mob no ."
            }),
            email:ko.observable(""),
            registerSubmit : function(){
                //var json = {"name":this.name,"mobile":this.mobile,"email":this.email};
                //var response = Playwin.core.registerUser(json);
                if(this.name.hasError() || this.mobile.hasError()){
                    return false;
                }
                $.ajax({
                    type : "GET",
                    url:Playwin.config.urls.registration,
                    data:"name="+this.name()+"&mobile="+this.mobile()+"&email="+this.email()   			
                			
                }).done(function(xmlDoc) {
                    var str = "<?xml version='1.0' standalone='yes'?><Registration><Name>"+registerViewModel.name()+"</Name><Mobile>"+registerViewModel.mobile()+"</Mobile><Email>"+registerViewModel.email()+"</Email><OTP>1234</OTP></Registration>";
                    var xmlDoc = $.parseXML( str );
                    var successData = $.xml2json(xmlDoc);
                    if(successData == "INVLAID INPUT"){
                        alert("INVLAID INPUT");
                    }else{
                        window.localStorage.setItem("otp",successData.OTP);
                				
                        window.localStorage.setItem("name",successData.Name);
                        window.localStorage.setItem("mobile",successData.Mobile);
                        window.localStorage.setItem("email",successData.Email);
                        window.localStorage.setItem("otp",successData.OTP);
                        window.localStorage.setItem("otpVarify",true);
                        Playwin.core.userDetails.name = successData.Name;
                        Playwin.core.userDetails.email = successData.Email;
                        Playwin.core.userDetails.mobile = successData.Mobile;
                        Playwin.core.userDetails.otp = successData.OTP;
                				
                        AppNamespace.app.navigate("registerOtpSubmit/"+successData.Mobile);///" + successData.Mobile+"/"+successData.OTP
                    }                			
                }).fail(function() {
                    alert("Error : Registration failed .");			
                });
                		
            }
               	
        };
       	
        return registerViewModel;
    };


    AppNamespace.registerOtpSubmit = function (params) {
        var registerViewModel = {
            message: ko.observable('Playwin User Registration !'),
            userName:Playwin.core.userDetails.name,
            userMobile:Playwin.core.userDetails.mobile,
            userOtp:ko.observable(""),
            reRegister:function(){
                AppNamespace.app.navigate("registerUser");
            },
            otpSubmit : function(){
                if(this.userOtp() == window.localStorage.getItem("otp")){
                    alert("You have successfully registered with playwin !");
                    window.localStorage.setItem("otpVarify" , "YES" );
                    AppNamespace.app.navigate("gamesList");
                }else{
                    alert("Please enter correct OTP .")
                }
        		
            }           	
        };
   	
        return registerViewModel;
    };


    AppNamespace.results = function () {
        var resultViewModel = {
            message: ko.observable('Playwin Results'),
            alertData:function(){
                alert(resultViewModel.dataSource().toSource());
            },
            dataSource: {
                load: function (loadOptions) {
                    if (loadOptions.refresh) {
                        var resultData=[];
                        var deferred = new $.Deferred();
            			
                        $
                        .get(Playwin.config.urls.results+"?gameid=1&sdate=01-jul-2013&edate=20-jul-2013")
                        .done(function (xmlData) {

                            var jsonObj = $.xml2json(xmlData);
                            //alert(jsonObj.toSource());
                            resultData = jsonObj.GameResult;
                            var mapped = $.map(resultData, function (data) {
                                return {
                                    Game: data.Game,
                                    Result: data.Result,
                                    DrawDate: Playwin.core.getFormatedDateTime(data.DrawDate,"dddd, dd-MMM-yyyy hh:mm TT"),
                                    NextDrawDate: Playwin.core.getFormatedDateTime(data.NextDrawDate,"dddd, dd-MMM-yyyy hh:mm TT"),
                                    NextFirstPrize:data.NextFirstPrize
		                            
                                }
                            });
                            deferred.resolve(mapped);
		            		
                        });
                        //alert(resultData.toSource());
                        return deferred;
	            		
                    }
                }
            } 
	
        };
	
        return resultViewModel;
    };

});




