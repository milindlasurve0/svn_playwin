/*
 *game play script for Playwin android app
 */

var Playwin = (typeof Playwin !== undefined) ? Playwin : {};

Playwin.submitCard = {
    ticketsJson :Playwin.play.jsonDetails/* {
		currJackpot : {
			id : "1",
			name : "Saturday Super Lotto",
			amt : "2.09 cr",
			displayName : "Saturday Super Lotto( 2.09 cr )"
		},
		nextDraw : "Tuesday, 03-Sep-2013 10:00 PM",
		noOfDraw : "1",
		draw : {
			id : "0",
			name : "Tuesday, 03-Aug-2013 10:00 PM"
		},
		tickets : [ {
			lp_flag : true,
			values : [ "0", "0", "0", "0", "0", "0" ]
		}, {
			lp_flag : false,
			values : [ "1", "2", "3", "4", "5", "6" ]
		} ]
	}*/,
    init : function() {
        // Render ticket and selected details
		
        var rowStr = "";
        $.each(Playwin.submitCard.ticketsJson.tickets, function(i, t) {
            rowStr = rowStr + "<tr id='row_" + (i + 1) + "'>";
            rowStr = rowStr + "<td style='width:80px;' align='left'>"
            + "<strong>Tickets " + (i + 1) + "</strong>" + "</td>";

            $.each(t.values, function(j, tv) {
                rowStr = rowStr + "<td style='width:30px;' align='center'>"
                + "<label id='lbtp" + (i + 1) + "t" + (j + 1)
                + "'   class='lottotbox lbc" + (i + 1) + "' >" + tv
                + "</label>" + "</td>";
            });
            rowStr = rowStr + "</tr>";
        });
        $("#tickets_table").append(rowStr);

        // bind submit card event
        $(document)
        .on(
            "click",
            "#submit_card",
            function() {
                var no = $("#cardNo").val();
                var pass = $("#pinNo").val();
                if (!no || $.trim(no) == "") {
                    alert("Card Number can not be empty .");
                    return false;
                } else if (!pass || $.trim(pass) == "") {
                    alert("Card Pin can not be empty .");
                    return false;
                } else if (!confirm("Amount of Rs. "+Playwin.play.jsonDetails.totalTicketPrice+" will be deducted from your card balance. Do you want to continue ?")) {
                    return false;
                }
                var dataStr = "";
                var betStr = "";
                var betArr = [];
                $.each(Playwin.submitCard.ticketsJson.tickets,
                    function(i, val) {
                        betArr.push(val.values.map(function(v) {
                            return v;
                        }));
                    })
                betStr = betArr.join("|");

                dataStr = "ItzNumber="
                + $("#cardNo").val()
                + "&ItzPassword="
                + $("#pinNo").val()
                + "&BetString="
                + betStr
                + "&GameID="
                + Playwin.submitCard.ticketsJson.currJackpot.id
                + "&DrawPointer="
                + Playwin.submitCard.ticketsJson.draw.id
                + "&TotalDraw="
                + Playwin.submitCard.ticketsJson.noOfDraw;
                $.ajax({
                    url : Playwin.config.urls.betPlacement,
                    method : "POST",
                    data : dataStr,
                    success : function(xmlData) {
                        var jsonObj = $.xml2json(xmlData);
                        // alert(jsonObj.toSource());
                        if (jsonObj.Status == "-1") {// Error : - if invalid card data
                            alert(jsonObj.StatusText);
                        } else if (jsonObj.Status == "0") {
                            if (jsonObj.Ticket.Status == "35") { // Error :- if invalid panelData
                                alert(jsonObj.Ticket.StatusText);
                            } else {
                                // success
                            }
                        }
                    },
                    complete : function(data) {

                    }
                })
            });

    },
    loadData : function(default_game) {

    }
}

$(function() {

    //Playwin.submitCard.init();

    });
