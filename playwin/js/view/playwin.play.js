/*
 *game play script for Playwin android app
 */

var Playwin = (typeof Playwin !== undefined) ? Playwin : {};

Playwin.play = {
    selectedGame : 0,
    playGameResponse : {
        error:"",
        tickets:[]
		
    },
    toast: {
        successMsg:"Success",
        errorMsg:"Some Error Occurred !",
        showInfo: function() { 
            var toast = $("#toast-info").data("dxToast");
            toast.show();
        },
        showError: function() {
            var toast = $("#toast-error").data("dxToast");
            toast.show();
        },
        showSuccess: function() {
            var toast = $("#toast-success").data("dxToast");
            toast.show();
        },
        showWarning: function() {
            var toast = $("#toast-warning").data("dxToast");
            toast.show();
        },
        showCustom: function() {
            var toast = $("#toast-custom").data("dxToast");
            toast.show();
        }
    },
    calculateTotalBetAmount:function(){
        var sum = 0;
        $(".ticket_amt").each(function() {
            sum = sum + parseInt(this.value*10);
        });
        sum = sum * parseInt($("#no_draw").val()==""?1:$("#no_draw").val());
        $("#sum_amt").val(sum);
        $("#sum_amt_text").html(sum);
        return sum;
    },
    
    defaultWheelPreset:{
        preset: 'mypreset',                    
        width: 5,
        theme: 'default',//$('#theme').val(),
        mode: 'scroller',//$('#mode').val(),
        lang: '',//$('#language').val(),
        display: 'bottom',// $('#display').val(),
        //scrollLock:true,
        animate: 'swing',// $('#animation').val()
        height:30,
        onClose:function(valueText, btn, inst) {
                                	                                	
        },
        onCancel:function(valueText, inst){
            var checkId = "";
                                   
            checkId = "#row_lb_1";
                                   
        },
        onSelect:function(valueText, inst) {
                                	
            var val = inst.getValue();
            var sorted_arr = val.slice(0);
                                	
            if(Playwin.play.selectedGame == 2){//for thunder ball
                sorted_arr.pop();
            }
                                	
                                	
            sorted_arr = sorted_arr.sort(); // You can define the comparing function here. 
            // JS by default uses a crappy string compare.
            var flag=false,results = [];
                                                                        
            for (var i = 0; i < sorted_arr.length - 1; i++) {
                if (sorted_arr[i]!=0 && sorted_arr[i + 1] == sorted_arr[i]) {
                    flag = true;
                    break;
                }
            }
            if(flag){
                var res = DevExpress.ui.dialog.alert("Please select UNIQUE Ticket Values .","Error!");
                res.done(function (dialogResult) {
                    inst.show();
                    return false;
                });                                    	
                                    	
            }else{
                                    	
                var checkId = "";
                var gameBalls = []
                $.each(val,function(i,v){
                    gameBalls.push(v==0?"LP":v);
                });
               viewPlayGameModel.gameBalls(gameBalls);
                                    	
            }
                                    
                                    
                                	
        }
    },
    go:function(gid){
                               
        var no = $("#cardNo").val();
        var pass = $("#pinNo").val();
        if(gid != 9){
            if( $("#all_draws").val() == "" && $("#no_draw").val() == ""){
                DevExpress.ui.dialog.alert("Please select a Draw OR no of draw !","Error");
                return false;
            // }else if(  ){
            //  alert("Please select No of draw !");return false;
            }    
        }
        var sum = 0;
        $(".ticket_amt").each(function() {
            sum = sum + parseInt(this.value*10);
        })
                				
        if (!no || $.trim(no) == "") {
            //alert("Card Number can not be empty .");
            DevExpress.ui.dialog.alert("Card Number can not be empty !","Error");
            return false;
        } else if (!pass || $.trim(pass) == "") {
            //alert("Card Pin can not be empty .");
            DevExpress.ui.dialog.alert("Card Pin can not be empty .","Error");
            return false;
        }else if ($("#all_draws").val() == "" && $("#no_draw").val() == "") {
            //alert("Please select 'Draw' OR 'No Of Draw' .");
            DevExpress.ui.dialog.alert("Please select 'Draw' OR 'No Of Draw' .","Error");
            return false;
        }else if($("tbody#draws_table tr").length == 0){
            DevExpress.ui.dialog.alert("Please add at least one ticket !","Ticket Error!");
            return false;
        }
                				
        /* if($("tbody#draws_table tr").length <= 1 ){
                 					
                 				} */     
        //else{
        //---- check card balance ----
                				
        $.ajax({
            url : Playwin.config.urls.checkBalance,
            method : "GET",
            data : "accountno="+$.trim(no)+"&password="+$.trim(pass),
            success : function(xmlData) {
                //var str = "<?xml version='1.0' standalone='yes'?><CardDetails><Code>-1</Code><Error>Invalid Account Number or Password</Error></CardDetails>";
                //var xmlData = $.parseXML( str );
                						
                var jsonObj = $.xml2json(xmlData);
                						
                if(jsonObj.Error || (jsonObj.Code && ( jsonObj.Code == -1 || jsonObj.Code == 99 )) ){
                    DevExpress.ui.dialog.alert(jsonObj.Error,"Invalid Card Error");
                }else if(!jsonObj.RemainingAmount){
                    DevExpress.ui.dialog.alert("Some error occurred when getting card balance. ","Card Read Error!");
                }else if(jsonObj.RemainingAmount < parseInt($("#sum_amt").val())){
                    DevExpress.ui.dialog.alert("Insufficient balance to place this bet. <br/>( Balance = Rs."+jsonObj.RemainingAmount+" ) ","Low Balance!");
                }else{
                    //-------start confirm box --------
                    var sum_amt = $("#sum_amt").val();
                    var result = DevExpress.ui.dialog.confirm(" Total Amount Rs. "+sum_amt+" will be deducted from your card balance. Do you want to continue ?","Your Card Balance is "+jsonObj.RemainingAmount+".");
                    result.done(function (dialogResult) {
                        if (!dialogResult) {
                            return false;
                        }else{
                            //}
                            var dataStr = "";
                            var betStr = "";
                            var betArr = [];
                            //alert($(".ticket").val());
                            $(".ticket").each(function() {
                                betArr.push(this.value);
                            })
                                    				
                            betStr = betArr.join("|");

                            dataStr = "ItzNumber="
                            + $("#cardNo").val()
                            + "&ItzPassword="
                            + $("#pinNo").val()
                            + "&BetString="
                            + betStr
                            + "&GameID="
                            + Playwin.play.selectedGame;
                            if(Playwin.play.selectedGame != 9){
                                var drawPointer,totalDraw;
                                    				   
                                if($("#all_draws").val() != "" && $("#draw_index_"+$("#all_draws").val()).val()==0){ // 1st draw selected
                                    drawPointer  = 1;
                                    if($("#no_draw").val()==""){//if no_of_draw not selected
                                        totalDraw  = 1;
                                    }else{//if no_of_draw selected
                                        totalDraw  = $("#no_draw").val();
                                    }
                                }else if( $("#all_draws").val() != "" ){//if other then first Draw selected
                                    drawPointer = $("#draw_index_"+$("#all_draws").val()).val();
                                    totalDraw  = 1;
                                }else if($("#all_draws").val() == ""){// if not any Draw selected
                                    drawPointer  = 1;
                                    totalDraw  = $("#no_draw").val();
                                }
                                    				   
                                    				   
                                dataStr = dataStr + "&DrawPointer=" + drawPointer + "&TotalDraw=" + totalDraw; 
                            //alert(dataStr);//return false;
                            //dataStr = dataStr + "&DrawPointer=" + $("#all_draws").val() + "&TotalDraw=" +$("#no_draw").val(); 
                            }
                                    			   
                            //({Status:"0", StatusText:"SUCCESS", Ticket:{Status:"35", StatusText:"|DRAWALLREADYCLOSED?"}})
                                    				
                                    			   
                            $.ajax({
                                url : Playwin.config.urls.betPlacement,
                                method : "POST",
                                data : dataStr,
                                success : function(xmlData) {
                                    /*var jsonObj = $.xml2json(xmlData);
                                    						alert(jsonObj.toSource());
                                    						if (jsonObj.Status == "-1") {// Error : - if invalid card data
                                    							alert("Some Error Occured .");//alert(jsonObj.StatusText);
                                    						} else if (jsonObj.Status == "0") {
                                    							if (jsonObj.Ticket.Status == "35") { // Error :- if invalid panelData
                                    								alert(jsonObj.Ticket.StatusText);
                                    							} else {
                                    								// success
                                    							}
                                    						}*/
                                    //-----------------------
                                    						
                                    var res = "";
                                    //error without ticket
                                    //var str = '<?xml version="1.0" encoding="UTF-8"?><Response><Status>-103</Status><StatusText>Myplawin Card Card is not valid.[Invalid Account Number or Password ]</StatusText></Response>' ;
                                                              
                                    //var str =  '<?xml version="1.0" encoding="UTF-8"?><Response><Status>30011</Status><StatusText>Bet can not placed.Please check your ITZ Card Statement for more details.</StatusText></Response>';
                                    						 
                                    						 
                                    //multiple ticket success
                                    // var str = "<Response><Status>0</Status><StatusText>SUCCESS</StatusText><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>590</DrawID><TSN>7E19-38F9-B039-D033</TSN><DrawDate>18/07/2013 22:00:00</DrawDate><SLSN>202338209</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>591</DrawID><TSN>5071-CAAA-9302-D033</TSN><DrawDate>25/07/2013 22:00:00</DrawDate><SLSN>202338210</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>592</DrawID><TSN>675A-5E6A-69ED-B033</TSN><DrawDate>01/08/2013 22:00:00</DrawDate><SLSN>202338211</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><CardBalance>14700.00</CardBalance></Response>"
                                    //                                    						 var str = "<?xml version='1.0' encoding='UTF-8'?><Response><Status>0</Status><StatusText>SUCCESS</StatusText><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>582</DrawID><TSN>38FD-1864-0410-F3AD</TSN><DrawDate>24/09/2013 22:00:00</DrawDate><SLSN>201588957</SLSN><BetDate>09/24/2013 21:05</BetDate><GameName>Thunderball,,,,</GameName><Cost>20.00</Cost><Mrp>10.00</Mrp><Panel><LP>1</LP><BetLine>A: 15 18 19 23 37  TB 11 </BetLine></Panel><Panel><LP>0</LP><BetLine>B: 02 03 04 05 06  TB 07 </BetLine></Panel><Promotion></Promotion></Ticket>" +
                                    //                                    						 	"<Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>590</DrawID><TSN>7E19-38F9-B039-D033</TSN><DrawDate>18/07/2013 22:00:00</DrawDate><SLSN>202338209</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>591</DrawID><TSN>5071-CAAA-9302-D033</TSN><DrawDate>25/07/2013 22:00:00</DrawDate><SLSN>202338210</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>592</DrawID><TSN>675A-5E6A-69ED-B033</TSN><DrawDate>01/08/2013 22:00:00</DrawDate><SLSN>202338211</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket>" +
                                    //                                    						 		"" +
                                    //                                    						 		"<CardBalance>88.00</CardBalance></Response>";


                                    						 
                                    						 
                                    //one ticket success
                                    //var str = "<Response><Status>0</Status><StatusText>SUCCESS</StatusText><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>590</DrawID><TSN>7E19-38F9-B039-D033</TSN><DrawDate>18/07/2013 22:00:00</DrawDate><SLSN>202338209</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><CardBalance>14700.00</CardBalance></Response>"
                                                          				
                                                             	
                                    //error one ticket
                                    // var str = '<?xml version="1.0" encoding="UTF-8"?><Response><Status>0</Status><StatusText>SUCCESS</StatusText><Ticket><Status>35</Status><StatusText>|INVALIDPANELDATA</StatusText></Ticket></Response>';
                                                             
                                    //var xmlData = $.parseXML( str );// convert string to xml
                                    var jsonObj = $.xml2json(xmlData);
                                    //alert(jsonObj.toSource());
                                    var resJson = {
                                        errors:"",
                                        CardBalance:"",
                                        tickets:[]
                                    };
                                                 				
                                    var errFlag = false;
                                    var sucTickt = 0;
                                    if (jsonObj.Status != 0){//jsonObj.Status == "-103" || jsonObj.Status == "-1") {// Error : - if invalid card data
                                        res = res +" Error : "+jsonObj.StatusText;
                                        errFlag = true;
                                    } else if (jsonObj.Status == "0" && jsonObj.StatusText == "SUCCESS") { // if status text is success
                                        var flag = false;
                                        resJson.CardBalance = jsonObj.CardBalance;
                                        if(jsonObj.Ticket instanceof Array){// if multiple tickets
                                            $.each(jsonObj.Ticket,function(i,v){
                                                var temp = {
                                                    err:false,
                                                    errMsg:"",
                                                    details:v
                                                };
                                                if(v.Status && v.StatusText){
                                                    if (v.Status == "0" && v.StatusText == "SUCCESS"){
                                                        sucTickt ++;
                                                        temp.err = false;
                                                        temp.details = v;
                                                    }else{
                                                        res = res + "Error : "+ v.StatusText+"||" ;
                                                        temp.err = true;
                                                        temp.errMsg = v.StatusText;
                                                    }
                                                }else{
                                                    temp.err = true;
                                                    temp.errMsg = v.StatusText;
                                                    res = res + "Error : "+ v.StatusText+"||";
                                                }
                                                resJson.tickets.push(temp);
                                             								
                                            });
                                        }else{
                                            var v = jsonObj.Ticket;
                                            var temp = {
                                                err:false,
                                                errMsg:"",
                                                details:v
                                            };
                                            if(v.Status && v.StatusText){
                                                if (v.Status == "0" && v.StatusText == "SUCCESS"){
                                                    sucTickt ++;
                                                    temp.err = false;
                                         										
                                                //res = res + "Success : "+v.Panel.BetLine + " ( "+ v.Panel.LP +" )";
                                                }else{
                                                    res = res + "Error : "+ v.StatusText;
                                                    temp.err = true;
                                                    temp.errMsg = v.StatusText;
                                                }
                                            }else{
                                                res = res + "Error : "+ v.StatusText+"" ;
                                                temp.err = true;
                                                temp.errMsg = v.StatusText;
                                            }
                                            resJson.tickets.push(temp);
                                        }
                                         							
                                         							
                                         							
                                    }
                                    //alert(res);
                                    Playwin.play.playGameResponse = resJson;
                                    resJson = null;
                                    //alert(resJson.toSource());
                                    if(sucTickt > 0){
                                        window.localStorage.setItem("cardNo",$.trim(no));
                                        AppNamespace.app.navigate("playGameSuccess/"+Playwin.play.selectedGame);
                                    }else{
                                        //alert(res);
                                        DevExpress.ui.dialog.alert(res,"Error");
                                    }
                                         						
                                //return false;
                                    						
                                //---------------------
                                    						
                                },
                                complete : function(data) {

                                }
                            });
                            						
                            						
                        }
                    });
                // ---- end of confirm box ----
                							
                							
                }
            }
                					
        }).fail(function () {
            DevExpress.ui.dialog.alert("Error : Unable get card balance .","Error!");
        });
                				
    //--------------check card balance error--------------
                				
                					
                					
                					
                				
                            
    },
    jsonDetails : {
        currJackpot : {
            id : "",
            name : "",
            amt : "",
            displayName : ""
        },
        draw : {
            id : "",
            name : ""
        },
        nextDraw : "",
        noOfDraw : "",
        ticketPrice: 0,
        totalTicketPrice: 0,
        tickets : []
    },
    init : function(gid) {
    },
    loadData : function(default_game) {
        Playwin.play.selectedGame = default_game;
        // if(default_game="")default_game = "4" ;
        var no_draw = 7;
		

        // set current draw
        var game_details = eval("Playwin.config.gameDetails." + "g"
            + Playwin.play.selectedGame);
		
		
        $("#c_draw").html(game_details.name + "( " + game_details.amt + " )");
        $("#c_draw_name").html(game_details.name);
        Playwin.play.jsonDetails.currJackpot.id = Playwin.play.selectedGame;
        Playwin.play.jsonDetails.currJackpot.name = game_details.name;
        Playwin.play.jsonDetails.currJackpot.amt = game_details.amt;
        Playwin.play.jsonDetails.currJackpot.displayName = game_details.name + "( "
        + game_details.amt + " )";
        if(Playwin.play.selectedGame == "9"){
            Playwin.play.jsonDetails.ticketPrice = 0;
            $("#new_row_id").val(2);
        }else {			
            Playwin.play.jsonDetails.ticketPrice = game_details.ticketPrice;
            $("#new_row_id").val(1);
        }
		
        // ---------------------

		
        // for keno game
        if (Playwin.play.selectedGame == "9") {
            var no_spot_min = 2;
            var no_spot_max = 10;
            var amt_min = 10;
            var amt_max = 100;

            // render no of spot selection list
			

            // render no of spot input boxes
			
            $(document)
            .on(
                "change",
                "#amt_option",
                function() {
                    //alert("Hello");
								
                    $(".spot_option").removeClass("selected");
                    $("#spotli_"+spot_val).addClass("selected");
								
                    $(".spot_option").addClass("not_selected");
                    $("#spotli_"+spot_val).removeClass("not_selected");
								
								
                    // show prize structure table
                    //alert("hello");
                    var tblStr = "<table><thead><td>Match</td><td>Prize</td></thead><tbody id='prize_structure'></tbody></table>";
                    $("#prize_amt_div").html(tblStr);
                    tblBody = "";
                    var matchPrz = Playwin.play.getPrizeStructure(
                        $("#spot_option").val(), $(
                            "#amt_option").val());
                    //alert(matchPrz.toSource());
                    $.each(matchPrz, function(i, val) {
                        tblBody = tblBody + "<tr><td>" + val.match
                        + "</td><td>" + val.prize
                        + "</td></tr>";
                    })
                    $("#prize_structure").html(tblBody);

                });


        }else{
			
    //Playwin.play.addTicketRow(Playwin.play.selectedGame,1);
    }

    },
    getPrizeStructure : function(spot, amt) {
        //alert(spot);
        spot = parseInt(spot);
        amt = parseInt(amt);
        var structure = [];
				
        if( !(spot=="" || amt=="") ){
                	
            var ps = eval("Playwin.config.gameDetails.g9.prizeStructure." + "s"+ spot);
            $.each(ps, function(i, val) {
                var temp = {
                    match : "",
                    prize : ""
                };
                temp.match = i.split("m")[1];
                temp.prize = val * amt * 10;
                temp.prize = temp.prize >= 6000000 ? 6000000 : temp.prize;
                structure.push(temp);
            });
                    
        }
				
        return structure;
    },
    removeTicket:function(id){
        //$('#ticket_'+id).scroller('destroy');
        $("#row_" + id).remove();
            
        $.each($(".tckt_no"),function(i,ele){
            $("#"+this.id).html(i+1);
				
        })
        var sum = 0;
        $(".ticket_amt").each(function() {
            sum = sum + parseInt(this.value)*10;
        })
        $("#sum_amt").val(sum);
        $("#sum_amt_text").html(sum);
        return false;
    },
        
    addTicketRow : function(gid, id) {
		
		
        //var gameDetails = eval("Playwin.config.gameDetails.g" + gid);

        var whls = [];
        var lp = false;
        whls = Playwin.play.getGameWheels(gid,lp,'all');//alert(whls.toSource());
    	

        $('#ticket_'+id).scroller('destroy').scroller($.extend(
        {
            wheels: whls
        },
        Playwin.play.defaultWheelPreset
        ));
    // $('#ticket_'+id).mobiscroll('show');
	
		
		
		
		
		
    },
    setOverLayResults:function(data,viewModel){
		
        viewModel.overLayResults([]);
        var deferred = new $.Deferred();
        viewModel.loadPanelMsg("Loading...");
        viewModel.loadPanelVisible(true);
        
       //alert(viewModel.resultCount());
        $.get(Playwin.config.urls.results+"?gameid="+data.id+"&count="+viewModel.resultCount()) //01-jul-2013 //20-jul-2013
        //$.get(Playwin.config.urls.results+"?gameid="+data.id+"&sdate="+Playwin.core.getFormatedDateTime(viewModel.dateBoxFr(),"dd-MMM-yyyy",true)+"&edate="+Playwin.core.getFormatedDateTime(viewModel.dateBoxTo(),"dd-MMM-yyyy",true)) //01-jul-2013 //20-jul-2013
        .done(function (xmlData) {
    		
            viewModel.loadPanelVisible(false);
            viewModel.loadPanelMsg("Loading...");
            var jsonObj = $.xml2json(xmlData);
            xmlData = null;
            //alert(jsonObj.toSource());//if(resultData.length > 0){
            var resultData = (!jsonObj.GameResult) || (jsonObj.GameResult.length == 0)  ? [] : jsonObj.GameResult;
    		
            if(resultData.Game){
                resultData = [resultData];
            }
    		
            var mapped = $.map(resultData, function (data) {
                var rStr = $.trim(data.Result);
                var rArr = rStr.split(" ");
                var rHtml = "";
                $.each(rArr,function(ri,rv){
                    if($.trim(rv) != ""){
                        rHtml = rHtml + "<div "+(rv!="TB"?"class='resultBall'>":"style='float:left;padding-top:5px;'>")+rv+"</div>";										
                    }        		            							
                });
                return {
                    Game: data.Game,
                    Result: data.Result,
                    DrawID: data.DrawID,
                    ResultHtml:rHtml,
                    DrawDate: Playwin.core.getFormatedDateTime(data.DrawDate,"dddd, dd-MMM-yyyy hh:mm TT"),
                    NextDrawDate: Playwin.core.getFormatedDateTime(data.NextDrawDate,"dddd, dd-MMM-yyyy hh:mm TT"),
                    NextFirstPrize:data.NextFirstPrize
                    
                };
            });
            deferred.resolve(mapped);
            viewModel.overLayResults(mapped);
            viewModel.resultTitle(data.name);
            viewModel.overlayVisible(true);
        }).fail(function () {
            viewModel.loadPanelVisible(false);
            viewModel.loadPanelMsg("Loading...");
            DevExpress.ui.dialog.alert("Error : Unable get results info.","Error!");
        });
    //alert(resultData.toSource());
    	
    },
    validateTicketsOld : function(gid) {
        if($(".ticket").length <= 1 ){
            return {
                'status':false , 
                'msg':"Please Enter at least one ticket !"
            };
        }
        var tickets = [];
        var rows = [];
        var duplicate;
        var noInRange;
        var dup_rows;
        dup_rows = [];
        var gd = eval("Playwin.config.gameDetails." + "g" + gid);
        var notinrange_rows;
        notinrange_rows = [];
        var ret = {
            "status" : true,
            "dup_rows" : [],
            "notinrange_rows" : []
        };

		
        $.each($("input[name='row_id[]']"), function() {
            rows.push($(this).val());
        });
		
        $
        .each(
            rows,
            function(i, val) {// for each row of ticket

                var ticketData = $('input.lb' + val).map(
                    function() {
                        return this.value;
                    }).toArray();
                var ticketDet = {
                    "lp_flag" : $("#lp_row_" + val).prop("checked"),
                    "values" : ticketData
                };
                tickets.push(ticketDet);
							
                $
                .each(
                    gd.ballGroups,
                    function(n, gdet) {// iterate for
                        // each groups

                        var list_of_values = $(
                            'input.' + n + (val))
                        .map(function() {
                            return this.value;
                        }).toArray();
                        // alert('input.'+n+val);
                        // alert(list_of_values);
                        duplicate = false;
                        noInRange = true;

                        if (!$("#row_lb_" + val).prop(
                            "checked")
                        && Playwin.play.selectedGame != "9") { // if
                            // lucky
                            // pic
                            // then
                            // discard
                            // that
                            // row
                            // for
                            // validate
                            $
                            .each(
                                list_of_values,
                                function(j,
                                    x) {
                                    noInRange = parseInt(x) >= gdet.limit.min
                                    && parseInt(x) <= gdet.limit.max;
                                    duplicate = list_of_values
                                    .indexOf(x) !== list_of_values
                                    .lastIndexOf(x);
                                    if (duplicate) {
                                        dup_rows
                                        .push("r"
                                            + (i + 1)
                                            + n);
                                        return false;
                                    } else if (!noInRange) {
                                        notinrange_rows
                                        .push("r"
                                            + (i + 1)
                                            + n);
                                        return false;
                                    }
                                ;
                                });
                        }

                    });

            });

        // }

        ret.tickets = tickets;
        if (dup_rows.length > 0 || notinrange_rows.length > 0) {
            ret.status = false;
            ret.dup_rows = dup_rows;
            ret.notinrange_rows = notinrange_rows;
        }
        // alert(ret.toSource());
        return ret;

    },
    validateTickets : function(gid) {
        if($(".ticket").length < 1 ){
                    
            return {
                'status':false , 
                'msg':"Please Enter at least one ticket !"
            };
        }
        var tickets = [];
        var rows = [];
        var duplicate;
        var noInRange;
        var dup_rows;
        dup_rows = [];
        var gd = eval("Playwin.config.gameDetails." + "g" + gid);
        var notinrange_rows;
        notinrange_rows = [];
        var ret = {
            "status" : true,
            "dup_rows" : []
        //"notinrange_rows" : []
        };
                
        //var duplicate = false;
        var tn = 1;
        $.each($(".ticket"), function() {
            //rows.push($(this).val())
            var id =  $(this).attr('id') ;//alert(id);
            var arr = id.split("_");//alert(arr.toSource());
            var ticketDet = {
                "lp_flag" : $("#row_lb_" + arr[1]).prop("checked"),
                "values" : $(this).mobiscroll('getInst').getValue(),
                "groups":{}
                                        
            };
            var i = 0;
                        
            $.each( gd.ballGroups, function(n, gdet) {// iterate for each groups
                            
                ticketDet.groups[n] = {};
                ticketDet.groups[n]["values"] = [];
                ticketDet.groups[n]["dupls"] = false;
                $.each( gdet.balls, function(m, bdet) {// iterate for each groups
                    //alert(m);
                    ticketDet.groups[n]["values"].push(ticketDet.values[i]);
                                
                    if(gdet.allow_duplicate == 'no' && !$("#row_lb_"+arr[1]).prop("checked")){
                        ticketDet.groups[n]["dupls"] = ticketDet.groups[n]["dupls"] ? ticketDet.groups[n]["dupls"] : ticketDet.groups[n]["values"].indexOf(ticketDet.values[i]) != ticketDet.groups[n]["values"].lastIndexOf(ticketDet.values[i]);
                        ret.status = ret.status ? !ticketDet.groups[n]["dupls"] : ret.status;
                        if(!ret.status && $.inArray(tn,ret.dup_rows) == -1 ) ret.dup_rows.push(tn);
                    }
                    i++;
                });
                            
            });
                                
            tickets.push(ticketDet);
            tn++;
        });
		
		

        ret.tickets = tickets;
		
        return ret;
    },
    validateKenoRow : function(gid) {
        if($("#ticket_1").length < 1 ){
                    
            return {
                'status':false , 
                'msg':"Please select no of spot !"
            };
        }
        var tickets = [];
        var rows = [];
        var duplicate;
        var noInRange;
        var dup_rows;
        dup_rows = [];
        var gd = eval("Playwin.config.gameDetails." + "g" + gid);
        var notinrange_rows;
        notinrange_rows = [];
        var ret = {
            "status" : true,
            "dup_rows" : []
        //"notinrange_rows" : []
        };
                
        //var duplicate = false;
        var tn = 1;
        $.each($("#ticket_1"), function() {
            //rows.push($(this).val())
            var id =  $(this).attr('id') ;//alert(id);
            var arr = id.split("_");//alert(arr.toSource());
            var ticketDet = {
                "lp_flag" : $("#row_lb_" + arr[1]).prop("checked"),
                "values" : $(this).mobiscroll('getInst').getValue(),
                "groups":{}
                                        
            };
            var i = 0;
                        
            $.each( gd.ballGroups, function(n, gdet) {// iterate for each groups
                //alert(n);
                //alert($("#row_lb_"+arr[1]).prop("checked"));
                //alert($("#row_lb_"+arr[1]).checked);
                ticketDet.groups[n] = {};
                ticketDet.groups[n]["values"] = [];
                ticketDet.groups[n]["dupls"] = false;
                $.each( gdet.balls, function(m, bdet) {// iterate for each groups
                    //alert(m);
                    ticketDet.groups[n]["values"].push(ticketDet.values[i]);
                                
                    if(gdet.allow_duplicate == 'no' && !$("#row_lb_"+arr[1]).prop("checked")){
                        ticketDet.groups[n]["dupls"] = ticketDet.groups[n]["dupls"] ? ticketDet.groups[n]["dupls"] : ticketDet.groups[n]["values"].indexOf(ticketDet.values[i]) != ticketDet.groups[n]["values"].lastIndexOf(ticketDet.values[i]);
                        ret.status = ret.status ? !ticketDet.groups[n]["dupls"] : ret.status;
                        if(!ret.status && $.inArray(tn,ret.dup_rows) == -1 ) ret.dup_rows.push(tn);
                    }
                    i++;
                });
                            
            });
                                
            tickets.push(ticketDet);
            tn++;
        });
		
		

        ret.tickets = tickets;
        /*if (dup_rows.length > 0 || notinrange_rows.length > 0) {
			ret.status = false;
			ret.dup_rows = dup_rows;
			ret.notinrange_rows = notinrange_rows;
		}*/
        // alert(ret.toSource());
        return ret;
    },
    getGameWheels:function(gid,lp,grpName){
            
        //alert("Playwin.config.gameDetails.g" + gid);
        //var gameDetails = eval("Playwin.config.gameDetails.g" + this.gid);
        var gameDetails = eval("Playwin.config.gameDetails.g" + gid);
        var gameGroups =gameDetails.ballGroups;
        var whls = [];
        //alert(grpName);
        //var grpName = 'all';
        $.each(gameGroups, function(n, gd) {// iterate for each groups
            //alert(grpName);
            //alert(grpName != n);
            if((grpName != "all" && grpName != n) /* || ( gid==9 &&  n != 'spotVal')*/ ){
                return;
            }
            if(gd.show_in_balls == "yes"){
                $.each(gd.balls, function(m, bd) {// iterate for each ball in group
                    var wi = [];
                    wi[0] = {
                        label:"",//m
                        keys:[],
                        values:[]
                    };	
                    if(lp){
                        wi[0].keys.push("0");
                        wi[0].values.push("LP");
                    }else{
                        for(var i=gd.limit.min;i<=gd.limit.max;i++){
                            var x="";
                            x = i<10 ? "0"+i : i+"" ;
                            wi[0].keys.push(x);
                            wi[0].values.push(x);
                        }
                    }
                    whls.push(wi);            				
                });
            }
        });
        return whls;
    }
}
