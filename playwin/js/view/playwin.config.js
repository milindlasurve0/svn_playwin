/*
 *Core script for Playwin android app
 */
 
function getConfigPlainObj (){
    var x ;
    if( x = window.localStorage.getItem("configObj")){
        var json = JSON.parse(x);
        return json;
    }
    
    var configObj =   {
    
    urls : {
        gameDetails : "https://www.myplaywin.com/PlaywinDrawXML.aspx?GameID=0",
        betPlacement : "https://www.myplaywin.com/MobileAppBetPlaceLotto.aspx",
        registration : "https://www.myplaywin.com/UserAppRegistration.aspx",
        results : "js/results5.xml",//"https://www.myplaywin.com/AppResultXML.aspx",
        checkBalance: "https://www.myplaywin.com/MyplaywinCardBalance.aspx",
        winnersList: "https://www.myplaywin.com/PlaywinWinner.aspx",
        addMarketting:"js/addMarkettingApi.json"
    },
    ItzNumber : "000000",
    ItzPassword : "00000",
    //kenoSpots:["","2","3","4","5","6","7","8","9","10"],
    kenoSpots:[
	           
    {
        id:"02",
        spot:"2"
    },{
        id:"03",
        spot:"3"
    },{
        id:"04",
        spot:"4"
    },{
        id:"05",
        spot:"5"
    },{
        id:"06",
        spot:"6"
    },{
        id:"07",
        spot:"7"
    },{
        id:"08",
        spot:"8"
    },{
        id:"09",
        spot:"9"
    },{
        id:"10",
        spot:"10"
    }],
	
    kenoAmtOpts : [
    {
        id:"01",
        amt:"10"
    },
    {
        id:"02",
        amt:"20"
    },
    {
        id:"03",
        amt:"30"
    },
    {
        id:"04",
        amt:"40"
    },
    {
        id:"05",
        amt:"50"
    },
    {
        id:"06",
        amt:"60"
    },
    {
        id:"07",
        amt:"70"
    },
    {
        id:"08",
        amt:"80"
    },
    {
        id:"09",
        amt:"90"
    },
    {
        id:"10",
        amt:"100"
    }
    ],
    gameList : [
    {
        "id" : "2",
        "type" : "daily",
        "nos" : 6,
        "name" : "Thunder Ball",
        "amt" : "1.20 cr",
        "lastResult":{}
	
    },
    {
        "id" : "11",
        "type" : "daily",
        "nos" : 5,
        "name" : "Jaldi Five Double Lotto",
        "amt" : "10 lack",
        "lastResult":{}
    },
    {
        "id" : "1",
        "type" : "daily",
        "nos" : 6,
        "name" : "Thursday Super Lotto",
        "amt" : "3.36 cr",
        "lastResult":{}
    },
		
    {
        "id" : "5",
        "type" : "daily",
        "nos" : 5,
        "name" : "Jaldi Five Lotto",
        "amt" : "5 lack",
        "lastResult":{}
    },		
    {
        "id" : "4",
        "type" : "daily",
        "nos" : 6,
        "name" : "Saturday Super Lotto",
        "amt" : "2.09 cr",
        "lastResult":{}
    },
    {
        "id" : "9",
        "type" : "weekly",
        "nos" : 0,
        "name" : "Keno",
        "amt" : "",
        "lastResult":{}/*
						 * { "a1": "10", "a2": "20", "a3": "30", "a4": "40",
						 * "a5": "50", "a6": "60", "a7": "100", "a8": "80",
						 * "a9": "90" }
						 */
    },
    {
        "id" : "3",
        "type" : "weekly",
        "nos" : 5,
        "name" : "Fast Digit Lottery",
        "amt" : "4.35 lack",
        "lastResult":{}
    }

    ],
    gameDetails : {

        "g1" : {
            "id" : "1",
            "ticketPrice" : 10,
            "type" : "daily",
            "nos" : 6,
            "name" : "Thursday Super Lotto",
            "amt" : "3.36 cr",
            "noOption" : {
                "min" : 1,
                "max" : 49
            },
            "ballGroups" : {
                "grp1" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "yes",
                    "limit" : {
                        "min" : 1,
                        "max" : 49
                    },
                    "balls" : {
                        "b1" : "1",
                        "b2" : "2",
                        "b3" : "3",
                        "b4" : "4",
                        "b5" : "5",
                        "b6" : "6"
                    }
                }
            },
            "draws" : [],
            "weekDays" : [4]
			
        },
        "g2" : {
            "id" : "2",
            "ticketPrice" : 10,
            "type" : "daily",
            "nos" : 6,
            "name" : "Thunderball",
            "amt" : "1.20 cr",
            "tbNoOption" : {
                "min" : 1,
                "max" : 15
            },
            "ballGroups" : {
                "grp1" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "yes",
                    "limit" : {
                        "min" : 1,
                        "max" : 42
                    },
                    "balls" : {
                        "b1" : "1",
                        "b2" : "2",
                        "b3" : "3",
                        "b4" : "4",
                        "b5" : "5"
                    }
                },
                "grp2" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "yes",
                    "limit" : {
                        "min" : 1,
                        "max" : 15
                    },
                    "balls" : {
                        "b6" : "6"
                    }
                }
            },
            "draws" : [],
            "weekDays" : [2]
        },
        "g3" : {
            "id" : "3",
            "ticketPrice" : 10,
            "type" : "weekly",
            "nos" : 5,
            "name" : "Fast Digit Lottery",
            "amt" : "4.35 lack",
            "ballGroups" : {
                "grp1" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "yes",
                    "limit" : {
                        "min" : 1,
                        "max" : 31
                    },
                    "balls" : {
                        "b1" : "1",
                        "b2" : "2",
                        "b3" : "3",
                        "b4" : "4",
                        "b5" : "5"
                    }
                }
            },
            "draws" : [],
            "weekDays" : [1,2,3,4,5,6,7]
        },
        "g4" : {
            "id" : "4",
            "ticketPrice" : 10,
            "type" : "daily",
            "nos" : 6,
            "name" : "Saturday Super Lotto",
            "amt" : "2.09 cr",
            "ballGroups" : {
                "grp1" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "yes",
                    "limit" : {
                        "min" : 1,
                        "max" : 49
                    },
                    "balls" : {
                        "b1" : "1",
                        "b2" : "2",
                        "b3" : "3",
                        "b4" : "4",
                        "b5" : "5",
                        "b6" : "6"
                    }
                }
            },
            "draws" : [],
            "weekDays" : [6]
        },
        "g5" : {
            "id" : "5",
            "ticketPrice" : 10,
            "type" : "daily",
            "nos" : 5,
            "name" : "Jaldi Five Lotto",
            "amt" : "5 lack",
            "ballGroups" : {
                "grp1" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "yes",
                    "limit" : {
                        "min" : 1,
                        "max" : 36
                    },
                    "balls" : {
                        "b1" : "1",
                        "b2" : "2",
                        "b3" : "3",
                        "b4" : "4",
                        "b5" : "5"
                    }
                }
            },
            "draws" : [],
            "weekDays" : [5]
        },
        "g9" : {
            "id" : "9",
            "type" : "weekly",
            "nos" : 0,
            "name" : "Keno",
            "amt" : ""/*
						 * { "a1": "10", "a2": "20", "a3": "30", "a4": "40",
						 * "a5": "50", "a6": "60", "a7": "100", "a8": "80",
						 * "a9": "90" }
						 */,
            "ballGroups" : {

                "noOfSpot" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "no",
                    "limit" : {
                        "min" : 2,
                        "max" : 10
                    },
                    "balls" : {
                        "b1" : 1
                    }
                },
                "spotVal" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "yes",
                    "limit" : {
                        "min" : 1,
                        "max" : 80
                    },
                    "balls" : {// set after spot selection
                }
                },
                "noOfBet" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "no",
                    "limit" : {
                        "min" : 1,
                        "max" : 10
                    },
                    "balls" : { // set after spot selection

                }
                }
            },
            "prizeStructure" : {
                s2 : {
                    m2 : 14
                },
                s3 : {
                    m2 : 4,
                    m3 : 20
                },
                s4 : {
                    m2 : 2,
                    m3 : 5,
                    m4 : 60
                },
                s5 : {
                    m3 : 3,
                    m4 : 25,
                    m5 : 450
                },
                s6 : {
                    m3 : 2,
                    m4 : 9,
                    m5 : 80,
                    m6 : 500
                },
                s7 : {
                    m3 : 2,
                    m4 : 3,
                    m5 : 15,
                    m6 : 130,
                    m7 : 4000
                },
                s8 : {
                    m4 : 2,
                    m5 : 10,
                    m6 : 100,
                    m7 : 496.5,
                    m8 : 35000
                },
                s9 : {
                    m4 : 2,
                    m5 : 5,
                    m6 : 25,
                    m7 : 110,
                    m8 : 5500,
                    m9 : 75000
                },
                s10 : {
                    m4 : 1,
                    m5 : 3,
                    m6 : 12,
                    m7 : 100,
                    m8 : 1000,
                    m9 : 8000,
                    m10 : 210000
                }
            },
            "draws" : [],
            "weekDays" : [1,7]
        },
        "g11" : {
            "id" : "11",
            "type" : "daily",
            "nos" : 5,
            "name" : "Jaldi Five Double Lotto",
            "amt" : "10 lack",
            "ballGroups" : {
                "grp1" : {
                    "allow_duplicate" : "no",
                    "show_in_balls" : "yes",
                    "limit" : {
                        "min" : 1,
                        "max" : 49
                    },
                    "balls" : {
                        "b1" : "1",
                        "b2" : "2",
                        "b3" : "3",
                        "b4" : "4",
                        "b5" : "5",
                        "b6" : "6"
                    }
                }
            },
            "draws" : [],
            "weekDays" : [3]
        }

    },
    winnersList:function(year){
        var data = [ 
        /*  {
						year : 2013,
						src : "content/images/winners/1.gif",
						name : "Pahadsingh Lalsinghji Rajput",								
						Winner	:	"Saturday Super Lotto",							
						Date 	:	"05 Nov 2011",
						Prize	:	"2,20,00,000",
						WinningNo: 	"03 08 15 31 38 14",
						City 	: 	"Maharashtra"
					}*/ ];
        var list = [];
		
        /*$.each(data , function(i,v){
			if(v.year == year){
				list.push(v);
			}
		});
		*/
        $.ajax({
            url : Playwin.config.urls.winnersList,
            method : "GET",
            data : "year="+year,
            success : function(xmlData) {
                var jsonObj = $.xml2json(xmlData);
				
                $.each(jsonObj.Winner , function(i,v){
                    var objYr = v.WinnerDate.split("-");
                    if(objYr[2] == year){
						
                        var obj = {
                            year      : v.year,
                            src       : "https://images.myplaywin.com/"+v.ImageName,
                            name      : v.Name,								
                            Winner    : objYr[2],							
                            Date      : v.WinnerDate,
                            Prize     : v.WinnerPrize,
                            WinningNo :	v.WinnerNumber,
                            City 	  :	v.City
                        };
						
                        list.push(obj);
                    }
                });
				
            }
        }).fail(function(){
			
            });
		
        return list;
    }
	

};

window.localStorage.setItem("configObj",JSON.stringify(configObj));
return configObj;
}   
 
var Playwin = (typeof Playwin !== undefined) ? Playwin : {};
Playwin.config = getConfigPlainObj();
   