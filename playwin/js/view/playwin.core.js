/*
 *Core script for Playwin android app
 */

var Playwin = {};
Playwin.core = {
    userDetails:{
        name:"",
        mobile:"",
        email:""
    },
    getParseClass:function(s1,s2,s3){
        var res = Playwin.core.parseStr(s1) + " "+ Playwin.core.parseStr(s2) + " "+ Playwin.core.parseStr(s3) ;
    },callUrlInBrowser:function(d,c){
        window.open(d,"_system");
    },
    parseStr:function(str){
        var res = "";
        res = str.split(" ").join("1");//space
        res = res.split("(").join("2");//(
        res = res.split(")").join("3");//)
        res = res.split(".").join("4");//.
        res = res.split("[").join("5");//[
        res = res.split("]").join("6");//]
        //alert(res);
        return res;
    },
	
    init : function(viewModel) {
        //Playwin.core.loadResultData("call_navigate");
	console.log("Inside Core INIT .");
        Playwin.core.loadResultData({
            //navigateTo:"Home",
            //viewModel:viewModel
        });
        
        Playwin.core.loadAdsData({});
        AppNamespace.app.navigate("Home");
    },
    exit :function(){
    //navigator.app.exitApp();
    },
    loadAdsData : function(params) {
        //var resultData=[];
        if(params.viewModel){
            params.viewModel.loadPanelVisible(true);
        }
        
        //var json = JSON.parse(window.localStorage.getItem("configObj"));
        //Playwin.config = json ? json : getConfigPlainObj ();
        //json = null
        
        $.ajax({
            url:Playwin.config.urls.addMarketting,
            method:"GET",
            dataType:"JSON"
            //data:"accountno="+$.trim(no)+"&password="+$.trim(pass),
            //success:
        })
        .done(function(xmlData){
                var jsonObj = xmlData;//$.xml2json(xmlData);
                xmlData = null;//for memory leak
                if(!jsonObj){
                    //DevExpress.ui.dialog.alert("Ads & Banner Load Error .","Error!");
                    return false;
                }
                $.each(jsonObj,function(i,v){
                    if(v.type == "image"){
                        window.localStorage.setItem("image"+v.id+"url" , v.imageUrl);
                        window.localStorage.setItem("image"+v.id+"rurl" , v.redirectUrl);
                    }else if(v.type == "text"){
                        window.localStorage.setItem("text"+v.id ,JSON.stringify(v.text) );
                    }  
                    
                    if(v.id == 6){str = v.text;advsData = null;return false;}
                });
                
                window.localStorage.setItem("advsMarketting",JSON.stringify(jsonObj));
                jsonObj = null;//for memory leak

                if(params.viewModel){
                    params.viewModel.loadPanelVisible(false);
                }
                if(params.navigateTo){
                    AppNamespace.app.navigate(params.navigateTo);
                }
        })
        .fail(function ( jqXHR, textStatus, errorThrown) {
            if(params.viewModel){
                params.viewModel.loadPanelVisible(false);
                //viewModel.loadPanelMsg("Loading...");
            }
            if(params.navigateTo){
                AppNamespace.app.navigate(params.navigateTo);
            }
            //$("#loadingDiv").hide();
            console.log("Error : Unable get ads & marketting info.");
            //DevExpress.ui.dialog.alert("Error : Unable get ads & marketting info.","Error!");
        });
        
       
    },
    loadResultData : function(params) {
        //alert("Load");//return;
        var resultData=[];
        var deferred = new $.Deferred();
        if(params.viewModel){
            params.viewModel.loadPanelVisible(true);
        }
        //params.viewModel.loadPanelMsg("Loading...");
        //viewModel.loadPanelVisible(true);
        
        var json = JSON.parse(window.localStorage.getItem("configObj"));
        Playwin.config = json ? json : getConfigPlainObj ();
        json = null;
        $.get(Playwin.config.urls.results)
        .done(function (xmlData) {
            $("#loadingDiv").hide();
            var jsonObj = $.xml2json(xmlData);
            xmlData = null;//for memory leak
            if(!jsonObj.GameResult){
                DevExpress.ui.dialog.alert("Game Results Load Error .","Error!");
                return false;
            }
            resultData = jsonObj.GameResult.length==0 ? [] : jsonObj.GameResult;
            jsonObj = null;//for memory leak
            //var mapped = $.map(resultData, function (data) {  
            
            $.each(resultData , function(l,data){
                var ret = {};//alert(Playwin.config.toSource());
                $.each(Playwin.config.gameList , function(i,obj){
                   
                    if(obj.name ==  data.Game){
                        Playwin.config.gameList[i].lastResult = {
                            "Game": data.Game,
                            "Result": data.Result,
                            "ResultHtml": "",
                            "DrawDate": Playwin.core.getFormatedDateTime(data.DrawDate,"ddd, dd-MMM-yyyy hh:mm TT"),
                            "NextDrawDate": Playwin.core.getFormatedDateTime(data.NextDrawDate,"ddd, dd-MMM-yyyy hh:mm TT"),
                            "NextDrawDateNF": data.NextDrawDate,
                            "NextFirstPrize":data.NextFirstPrize,
                            "RolldownAmount": data.RolldownAmount ? data.RolldownAmount : "",
                            "isMoreBalls":false
		                            
                        }
                        var rStr = $.trim(data.Result);
                        var rArr = rStr.split(" ");
                        var ballC = 0;
                        $.each(rArr,function(ri,rv){
                            if($.trim(rv) != ""){
                                ballC ++ ;
                                if(ballC == 8){
                                    Playwin.config.gameList[i].lastResult.isMoreBalls = true;
                                    Playwin.config.gameList[i].lastResult.ResultHtml = 
                                    "<div  style='float:left;position:relative; padding:0px;'></div>";
    										
                                }
                                if(ballC <= 7){
                                    Playwin.config.gameList[i].lastResult.ResultHtml = Playwin.config.gameList[i].lastResult.ResultHtml + 
                                    "<div "+(rv!="TB"?"class='resultBall'>":"style='float:left;padding-top:5px;'>")+rv+"</div>";
                                }
                            }        		            							
                        });
                        //alert(Playwin.config.gameList[i].lastResult.isMoreBalls);
                        
                        //return Playwin.config.gameList[i] ;
                    }
                });
    			
            //return Playwin.config.gameList[i] ;
            });            
            window.localStorage.setItem("configObj",JSON.stringify(Playwin.config));
            resultData = null;//for memory leak
            
            //Playwin.config = null;
            Playwin.config ={urls : Playwin.config.urls};
            //json = null;
            
            if(params.viewModel){
                params.viewModel.loadPanelVisible(false);
            }
            if(params.navigateTo){
                AppNamespace.app.navigate(params.navigateTo);
            }
    		
            
        }).fail(function ( jqXHR, textStatus, errorThrown) {
            if(params.viewModel){
                params.viewModel.loadPanelVisible(false);
                //viewModel.loadPanelMsg("Loading...");
            }
            if(params.navigateTo){
                AppNamespace.app.navigate(params.navigateTo);
            }
            $("#loadingDiv").hide();
            DevExpress.ui.dialog.alert("Error : Unable get results info.","Error!");
        });
		
    //return deferred;
    },
    getFormatedDateTime : function(str, op_format ,isDtObj) {// this function accepts the
        // format "dd/mm/yyyy
        // hh:mm:ss" , op_format =
        // output format "dddd,
        var dt;									// dd-MMM-yyyy hh:mm TT"
        if(isDtObj){
            dt = str
        }else{
            if (op_format == null || op_format == "") {
                op_format = "dddd, dd-MMM-yyyy hh:mm TT";
            }
            var dtStr = str.split(" ");
            var dtStrArr = dtStr[0].split("/");
            var tmStrArr = dtStr[1].split(":");
            var dt = new Date(dtStrArr[2], dtStrArr[1] - 1, dtStrArr[0], tmStrArr[0],tmStrArr[1], tmStrArr[2], 0);
            dtStrArr = null;
            tmStrArr = null;
        }
	
        return ($.fullCalendar.formatDate(dt, op_format));// -> String;
    },
    registerUser:function(jsonData){
		
		
    },
    backKeyDown:function(jsonData){
        AppNamespace.app.navigate("#_back");
    },
    /*,
        TimerPlugin:{
            ////////////
            Timer:null,
            TotalSeconds:null,
            ////get test start time
            ////////////////timer function for
            createTimer:function(TimerID, Time) {

                Playwin.core.TimerPlugin.Timer = document.getElementById(TimerID);
               
                Playwin.core.TimerPlugin.TotalSeconds = Time;

                Playwin.core.TimerPlugin.UpdateTimer()
                window.setTimeout("Playwin.core.TimerPlugin.Tick()", 1000);
            },

            Tick:function() {
                ///showing time up message when count goes 0
                if ( Playwin.core.TimerPlugin.TotalSeconds <= 0) {
                    
                    //alert("Your Time's up!")
                    // location.href = Pratiyogi.config.baseUrl.onlineTestResultpage
                    return false;
                }
                /////adding a ticker to tick every one second
                Playwin.core.TimerPlugin.TotalSeconds -= 1;
                Playwin.core.TimerPlugin.UpdateTimer()
                window.setTimeout("Playwin.core.TimerPlugin.Tick()", 1000);
            },

            UpdateTimer:function() {
                var Seconds =  Playwin.core.TimerPlugin.TotalSeconds;
                //converting seconds to min, min to hours and days
                var Days = Math.floor(Seconds / 86400);
                Seconds -= Days * 86400;

                var Hours = Math.floor(Seconds / 3600);
                Seconds -= Hours * (3600);

                var Minutes = Math.floor(Seconds / 60);
                Seconds -= Minutes * (60);

                ///putting the extracted data to the given id
                var TimeStr = ((Days > 0) ? Days + " days " : "") + Playwin.core.TimerPlugin.LeadingZero(Hours) + ":" + Playwin.core.TimerPlugin.LeadingZero(Minutes) + ":" + Playwin.core.TimerPlugin.LeadingZero(Seconds)

                Playwin.core.TimerPlugin.Timer.innerHTML = TimeStr;
            },

            //adding 0 to the hours and min if the number is less than 0
            LeadingZero:function(Time) {
                return (Time < 10) ? "0" + Time : + Time;
            }
        }*/
        
    TimerPlugin:{
        ////////////
        Timer:[],
        TotalSeconds:[],
        State:"",
        ////get test start time
        ////////////////timer function for
        createTimer:function(TimerID, Time) {
            	
            Playwin.core.TimerPlugin.Timer.push(document.getElementById(TimerID));
            //alert(Time);
            Playwin.core.TimerPlugin.TotalSeconds.push(Time);
        // Playwin.core.TimerPlugin.TotalSeconds = Time;
        //alert(Playwin.core.TimerPlugin.TotalSeconds.toSource());
        // Playwin.core.TimerPlugin.UpdateTimer()
        // window.setTimeout("Playwin.core.TimerPlugin.Tick()", 1000);
        },
        Start:function() {
            //alert(Playwin.core.TimerPlugin.TotalSeconds.toSource());
            if(Playwin.core.TimerPlugin.State != "Running"){
                Playwin.core.TimerPlugin.State = "Running";
                Playwin.core.TimerPlugin.Tick();            		
            }
            	
        },
        Tick:function() {
            ///showing time up message when count goes 0
            $.each(Playwin.core.TimerPlugin.TotalSeconds ,function(i,v){
                if ( Playwin.core.TimerPlugin.TotalSeconds[i] <= 0) {
                    //alert("Your Time's up!")
                    // location.href = Pratiyogi.config.baseUrl.onlineTestResultpage
                    return true;
                }
                /////adding a ticker to tick every one second
                Playwin.core.TimerPlugin.TotalSeconds[i] -= 1;
                Playwin.core.TimerPlugin.UpdateTimer(i);
            });
              
            if(Playwin.core.TimerPlugin.State == "Running"){
                window.setTimeout("Playwin.core.TimerPlugin.Tick()", 1000);
            }        
                
        },

        UpdateTimer:function(i) {
            // alert(i);
            var Seconds =  Playwin.core.TimerPlugin.TotalSeconds[i];
            //converting seconds to min, min to hours and days
            var Days = Math.floor(Seconds / 86400);
            Seconds -= Days * 86400;

            var Hours = Math.floor(Seconds / 3600);
            Seconds -= Hours * (3600);

            var Minutes = Math.floor(Seconds / 60);
            Seconds -= Minutes * (60);

            ///putting the extracted data to the given id
            var TimeStr = ((Days > 0) ? Days + " days " : "") + Playwin.core.TimerPlugin.LeadingZero(Hours) + ":" + Playwin.core.TimerPlugin.LeadingZero(Minutes) + ":" + Playwin.core.TimerPlugin.LeadingZero(Seconds)
                
            Playwin.core.TimerPlugin.Timer[i].innerHTML = TimeStr;
        },

        //adding 0 to the hours and min if the number is less than 0
        LeadingZero:function(Time) {
            return (Time < 10) ? "0" + Time : + Time;
        }
    }
}

//40 mins = 2400
//Playwin.core.TimerPlugin.createTimer("timer", totl_time_test);
//document.addEventListener("backbutton", backKeyDown, true);
