AppNamespace.Home=function(d){
    
    
    
    var c={
        tbItems:[{
            align:"left",
            template:"nav-button"
        },{
            text:"Playwin Home",
            align:"center"
        }],
        get:function(){
            return $(window).width()*0.9
        },
        bannerImgUrl:ko.observable(window.localStorage.getItem("image2url")),
        bannerImgRUrl:ko.observable(window.localStorage.getItem("image2rurl")),
        loadPanelVisible:ko.observable(false),
        loadPanelMsg:ko.observable("Loading..."),
        socialApplink:ko.observable(""),
        isShowSocialApp:ko.observable(false),
        showSocialApp:function(a,b){
            this.isShowSocialApp(true);
            this.socialApplink(a);
            window.open(a,"_system")
        },
        openBannerRUrl:function(a,b){
            window.open(a,"_system")
        },
        hideSocialApp:function(){
            this.isShowSocialApp(false)
        },
        navigate:function(a,b){
            if(a=="LottoGames"){
                AppNamespace.app.navigate("gamesList/play/daily")
            }else{
                if(a=="WeeklyGames"){
                    AppNamespace.app.navigate("gamesList/play/weekly")
                }else{
                    if(a=="Results"){
                        AppNamespace.app.navigate("gamesList/results/daily")
                    }else{
                        if(a=="TodaysJackpot"){
                            AppNamespace.app.navigate("gamesList/play/today")
                        }else{
                            if(a=="MyAccount"){
                                AppNamespace.app.navigate("myAccount")
                            }else{
                                if(a=="SocialConnect"){
                                    this.isShowSocialApp(true)
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    return c
};

ko.extenders.required=function(f,d){
    f.hasError=ko.observable();
    f.validationMessage=ko.observable();
    function e(a){
        f.hasError(a?false:true);
        f.validationMessage(a?"":d||"This field is required")
    }
    e(f());
    f.subscribe(e);
    return f
};
    
ko.extenders.mobile=function(f,d){
    f.hasError=ko.observable();
    f.validationMessage=ko.observable();
    function e(h){
        var c=d;
        var b=false;
        var a="";
        a=h.toString();
        if(isNaN(a)||a.indexOf(" ")!=-1){
            c="";
            b=true
        }else{
            if(a.length!=10){
                c="";
                b=true
            }
        }
        f.hasError(b);
        f.validationMessage(b?c:"")
    }
    e(f());
    f.subscribe(e);
    return f
};
    
ko.extenders.numeric=function(e,f){
    var d=ko.computed({
        read:e,
        write:function(k){
            var a=e(),b=Math.pow(10,f),j=isNaN(k)?0:parseFloat(+k),c=Math.round(j*b)/b;
            if(c!==a){
                e(c)
            }else{
                if(k!==a){
                    e.notifySubscribers(c)
                }
            }
        }
    });
    d(e());
    return d
};

ko.extenders.email=function(f,d){
    f.hasError=ko.observable();
    f.validationMessage=ko.observable();
    function e(h){
        var c=d;
        var b=false;
        var a="";
        a=h.toString();
        if(a&&validateEmail(a)){
            c="";
            b=true
        }
        alert(b);
        f.hasError(b);
        f.validationMessage(b?c:"")
    }
    e(f());
    f.subscribe(e);
    return f
};
    
function validateEmail(d){
    var c=/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return c.test(d)
}
AppNamespace.registerUser=function(){
    var b={
        message:ko.observable(""),
        loadPanelVisible:ko.observable(false),
        loadPanelMsg:ko.observable("Loading..."),
        name:ko.observable("").extend({
            required:"Please enter your name."
        }),
        mobile:ko.observable("").extend({
            mobile:"Please enter correct mob no."
        }),
        email:ko.observable(""),
        registerSubmit:function(){
            if(this.name.hasError()){
                DevExpress.ui.dialog.alert("Error : Name should not be empty .","Error!");
                return false
            }else{
                if(this.mobile.hasError()){
                    DevExpress.ui.dialog.alert("Error : Mobile No should be a 10 Digit No .","Error!");
                    return false
                }else{
                    if(this.email()&&!validateEmail(this.email())){
                        DevExpress.ui.dialog.alert("Error : Please Enter a correct Email ID .","Error!");
                        return false
                    }
                }
            }
            b.loadPanelVisible(true);
            var dataStr = "name="+this.name()+"&mobileno="+this.mobile()+"&email="+this.email();
            if(typeof device != 'undefined'){
                dataStr = dataStr + "&device_name="+device.name+"&device_plateform="+device.plateform+"&device_uuid="+device.uuid+"&device_version="+device.version;
            }
            $.ajax({
                type:"GET",
                url:Playwin.config.urls.registration,
                data:dataStr
            }).done(function(a){
                b.loadPanelVisible(false);
                //a="<Registration><Name>name</Name><Mobile>9999999999</Mobile><Email>email</Email><OTP>1ee234</OTP></Registration>";
                var d=$.xml2json(a);
                //alert(d.toSource());
       
                if(d=="INVLAID INPUT" ){
                    DevExpress.ui.dialog.alert("Error : INVLAID INPUT .","Error!")
                }else  if(!d.OTP || d.OTP==undefined){
                    DevExpress.ui.dialog.alert("Error : Registration Failed.","SERVER Error!")
                } else{
               
                
                    setData("otp",d.OTP);
                    setData("name",d.Name);
                    setData("mobile",d.Mobile);
                    setData("email",d.Email);
            
            
               
               
               
                    //window.localStorage.setItem("otp",d.OTP);
                    //            window.localStorage.setItem("name",d.Name);
                    //            window.localStorage.setItem("mobile",d.Mobile);
                    //            window.localStorage.setItem("email",d.Email);
                    //            window.localStorage.setItem("otp",d.OTP);
                    window.localStorage.setItem("otpVarify",true);
                    Playwin.core.userDetails.name=d.Name;
                    Playwin.core.userDetails.email=d.Email;
                    Playwin.core.userDetails.mobile=d.Mobile;
                    Playwin.core.userDetails.otp=d.OTP;
                    AppNamespace.app.navigate("registerOtpSubmit/"+d.Mobile)
                }
            }).fail(function(){
                b.loadPanelVisible(false);
                DevExpress.ui.dialog.alert("Error : Registration failed .","Error!")
            })
        }
    };

    return b
};

AppNamespace.registerOtpSubmit=function(c){
    var d={
        message:ko.observable("Playwin User Registration !"),
        loadPanelVisible:ko.observable(false),
        loadPanelMsg:ko.observable("Loading..."),
        userName:Playwin.core.userDetails.name,
        userMobile:Playwin.core.userDetails.mobile,
        userOtp:ko.observable(""),
        reRegister:function(){
            //AppNamespace.app.navigate("registerUser")
            $.ajax({
                type:"GET",
                url:Playwin.config.urls.registration,
                data:"name="+getData("name")+"&mobileno="+getData("mobile")+"&email="+getData("email")+"&device_name="+device.name+"&device_plateform="+device.plateform+"&device_uuid="+device.uuid+"&device_version="+device.version
            }).done(function(a){
                d.loadPanelVisible(false);
                var dd=$.xml2json(a);
                if(dd=="INVLAID INPUT"){
                    DevExpress.ui.dialog.alert("Error : INVLAID INPUT .","Error!")
                }else{
                            
                            
                    setData("otp",dd.OTP);
                    setData("name",dd.Name);
                    setData("mobile",dd.Mobile);
                    setData("email",dd.Email);
                            
                    //                            window.localStorage.setItem("otp",dd.OTP);
                    //                            window.localStorage.setItem("name",dd.Name);
                    //                            window.localStorage.setItem("mobile",dd.Mobile);
                    //                            window.localStorage.setItem("email",dd.Email);
                    //                            window.localStorage.setItem("otp",dd.OTP);
                    window.localStorage.setItem("otpVarify",true);
                    Playwin.core.userDetails.name=dd.Name;
                    Playwin.core.userDetails.email=dd.Email;
                    Playwin.core.userDetails.mobile=dd.Mobile;
                    Playwin.core.userDetails.otp=dd.OTP;
                    AppNamespace.app.navigate("registerOtpSubmit/"+dd.Mobile)
                }
            }).fail(function(){
                b.loadPanelVisible(false);
                DevExpress.ui.dialog.alert("Error : Registration failed .","Error!")
            })
        },
        otpSubmit:function(){
           
            if(this.userOtp()==getData("otp")){
                var a=DevExpress.ui.dialog.alert("You have successfully registered with playwin !","Congrats!");
                a.done(function(b){
                    window.localStorage.setItem("otpVarify","YES");
                    window.localStorage.setItem("otp"," ");
                    Playwin.core.init(d)
                })
            }else{
                DevExpress.ui.dialog.alert("Error : Please enter correct OTP .","Error!")
            }
        }
    };

    return d
};

AppNamespace.gamesList=function(params){
    viewPlayGameModel=null;
    var viewModel={
        type:params.type,
        dayType:params.dayType,
        bannerImgUrl:params.type=="results"?ko.observable(window.localStorage.getItem("image3url")):ko.observable(window.localStorage.getItem("image4url")),
        pastResultBannerUrl:ko.observable(window.localStorage.getItem("image7url")),
        pastResultBannerRUrl:ko.observable(window.localStorage.getItem("image7rurl")),
        bannerImgRUrl:params.type=="results"?ko.observable(window.localStorage.getItem("image3rurl")):ko.observable(window.localStorage.getItem("image4rurl")),
        title:function(){
            var t;
            if(params.dayType=="daily"){
                t="Lotto Games"
            }else{
                if(params.dayType=="weekly"){
                    t="Weekly Games"
                }else{
                    if(params.dayType=="today"){
                        t="Today's Draws"
                    }
                }
            }
            return t
        },
        btext:ko.observable("menu"),
        tbItems:[{
            align:"left",
            template:"nav-button"
        },{
            text:function(){
                var t;
                if(params.dayType=="daily"){
                    t="Lotto Games"
                }else{
                    if(params.dayType=="weekly"){
                        t="Weekly Games"
                    }else{
                        if(params.dayType=="today"){
                            t="Today's Draw"
                        }
                    }
                }
                return t
            }(),
            align:"left"
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"refresh",
                clickAction:function(){
                    Playwin.core.loadResultData({
                        // navigateTo:"Home",
                        viewModel:viewModel
                    })
                }
            }
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"home",
                clickAction:"#Home"
            }
        }],
        tabs:[{
            text:"Lotto"
        },{
            text:"Weekly"
        }],
        tabClicked:function(e){
            if(e.itemData.text=="Lotto"){
                viewModel.dayType="daily"
            }else{
                if(e.itemData.text=="Weekly"){
                    viewModel.dayType="weekly"
                }
            }
            var gameList=viewModel.getGameList();
            viewModel.dataSourceResult(gameList);
            window.setTimeout(
                function() {
                    if($(".timers").length==0){
                        window.setTimeout(
                            function() {
                                viewModel.setTimers(gameList);
                            }, 5000);
                    }else{
                        viewModel.setTimers(gameList);
                    }
                }, 3000);
        },
        selectedTab:ko.observable(0),
        isShowMoreBalls:ko.observable(false),
        moreBallsTitle:ko.observable("Keno Result"),
        moreBallsHtml:ko.observable(""),
        hideMoreBalls:function(data){
            viewModel.isShowMoreBalls(false)
        },
        openBannerRUrl:function(d,c){
            window.open(d,"_system")
        },
        showMoreBalls:function(data){
            var gid=data.id;
            var html="";
            $.each(Playwin.config.gameList,function(gli,glv){
                if(glv.id==gid){
                    var rStr=$.trim(glv.lastResult.Result);
                    var rArr=rStr.split(" ");
                    $.each(rArr,function(ri,rv){
                        if($.trim(rv)!=""){
                            html=html+"<div "+(rv!="TB"?"class='resultBall'>":"style='float:left;padding-top:5px;'>")+rv+"</div>"
                        }
                    })
                }
            });
            viewModel.moreBallsHtml(html);
            viewModel.isShowMoreBalls(true)
        },
        resultTitle:ko.observable(""),
        resultGameId:ko.observable(""),
        overlayVisible:ko.observable(false),
        overLayResults:ko.observable([]),
        resultCount:ko.observable(0),
        loadPanelVisible:ko.observable(false),
        loadPanelMsg:ko.observable("Loading ..."),
        selGameDataOverLay:ko.observable([]),
        dateBoxFr:ko.observable(new Date((new Date()).getTime()-((7+(new Date()).getDay())*24*60*60*1000))),
        dateBoxTo:ko.observable(new Date()),
        gameType:ko.observable(""),
        resultsHtml:ko.computed(function(){
            return"resultHtml"
        },this),
        toolbar:{
            items:[{
                align:"left",
                template:"nav-button"
            },{
                text:this.title,
                align:"center"
            },{
                align:"right",
                widget:"button",
                options:{
                    icon:"plus",
                    clickAction:function(e){
                        alert("plus click")
                    }
                }
            }]
        },
        getGameList:function(){
            var list=[];
            $.each(Playwin.config.gameList,function(i,v){
                if(!viewModel.getWeekStatus(viewModel.dayType,v)){
                    list.push(v)
                }
            });
            return list
        },
        playGameBtn:function(data){
            AppNamespace.app.navigate("playGame/"+data.id)
        },
        gameInfoBtn:function(data){
            AppNamespace.app.navigate("gameInfo/"+data.id)
        },
        myDivHandler:function(data){
        //    $("#passbook_"+data.id).fadeIn(600);
           
        $("#passbook_"+data.id).toggleClass("rotate-3dX")
         //alert(divNo);
        },
        showPopup:function(data){
            viewModel.setOverLayResults(0,0,0,0);
            viewModel.overlayVisible(true)
        },
        hidePopup:function(data){
            viewModel.overlayVisible(false)
        },
        myRotatorX:function(data){
            $("#passbook_"+data.id).toggleClass("rotate-3dX");
            $("#passbook_"+data.id).toggleClass("rev-rotate-3dX");
            
        },
        myRotatorY:function(data){},
        getFrontBackClass:function(type,divNo){
            
            var ret="";
            if(divNo==1){
                if(type=="play"||type=="results"){
                    if(type=="play"){
                        ret="front"
                    }else{
                        ret="back"
                    }
                }else{
                    ret="front"
                }
            }else{
                if(divNo==2){
                    if(type=="play"||type=="results"){
                        if(type=="results"){
                            ret="front"
                        }else{
                            ret="back"
                        }
                    }else{
                        ret="back"
                    }
                }
            }
            return ret
        },
        getWeekStatus:function(type,data){
            var gameDetails=eval("Playwin.config.gameDetails.g"+data.id);
            var val=0;
            val=new Date().getDay();
            var ret=true;
            if(type=="today"){
                if($.inArray(val,gameDetails.weekDays)!=-1){
                    ret=false
                }
            }else{
                if(type=="weekly"&&gameDetails.type!="weekly"){
                    ret=true
                }else{
                    if(type=="daily"&&gameDetails.type!="daily"){
                        ret=true
                    }else{
                        ret=false
                    }
                }
            }
            return ret
        },
        getWeekClasses:function(gid){
            var gameDetails=eval("Playwin.config.gameDetails.g"+gid);
            var weakClass="";
            weakClass=gameDetails.weakDays.join(" W");
            weakClass="W"+weakClass;
            return weakClass
        },
        listHeight:ko.observable(0),
        itemMargin:ko.observable(0),
        baseItemWidth:ko.observable(0),
        mySpanHandler:function(param,id){
            alert(param.toSource())
        },
        dataSource:Playwin.config.gameList,
        dataSourceNew : ko.observable([]),
        dataSourceResult :function() { //ko.observable(
            /*DevExpress.data
            .createDataSource({
                load : function(loadOptions) {
                    if (loadOptions.refresh) {*/
            //alert("CHECK");
            var resultData = [];
            var deferred = new $.Deferred();
            viewModel.loadPanelMsg("Loading...");
            viewModel.loadPanelVisible(true);
            var dataArr = this.getGameList();
                       
            viewModel.itemMargin(($(window).width() > $(window).height() ? $(window).height() * 0.05: $(window).width() * 0.05));
            viewModel.listHeight(dataArr.length * ((viewModel.itemMargin()) + 82)
                + viewModel.itemMargin());
            viewModel.baseItemWidth($(window).width()- (viewModel.itemMargin() * 2 + 5));
            deferred.resolve(dataArr);
            viewModel.dataSourceNew(dataArr);
            window.setTimeout(
                function() {
                    if($(".timers").length==0){
                        window.setTimeout(
                            function() {
                                viewModel.setTimers(dataArr);
                            }, 5000);
                    }else{
                        viewModel.setTimers(dataArr);
                    }
                }, 3000);
            viewModel.loadPanelVisible(false);
            viewModel.loadPanelMsg("Loading...");
                       
            return deferred;
        /* }
                }
            })*/},
        setTimers:function(dataArr){
                                    
            Playwin.core.TimerPlugin.Timer = [];
            Playwin.core.TimerPlugin.TotalSeconds = [];
            $(".timers")
            .each(
                function() { //alert(this.id);
                    var arr = this.id
                    .split("_");
                    if (arr[0] == "timer") {
                        var x = "";
                        var id;
                        $
                        .each(
                            dataArr,
                            function(
                                i,
                                v) {
                                if (v.id == arr[1]) {
                                    x = v.lastResult.NextDrawDateNF;
                                    id = v.id
                                }
                            });
                        if (x == undefined || x == "") {
                            //alert("uder");
                            return
                        }
                        //alert(x);
                        var dtStr = x
                        .split(" ");
                        var dtStrArr = dtStr[0]
                        .split("/");
                        var tmStrArr = dtStr[1]
                        .split(":");
                        var dt = new Date(
                            dtStrArr[2],
                            dtStrArr[1] - 1,
                            dtStrArr[0],
                            tmStrArr[0],
                            tmStrArr[1],
                            tmStrArr[2],
                            0);
                        var diff = parseInt((dt
                            .getTime() / 1000)
                        - (new Date()
                            .getTime() / 1000));
                        Playwin.core.TimerPlugin
                        .createTimer(
                            "timer_"
                            + id,
                            diff)
                    }
                });
            window.setTimeout("Playwin.core.TimerPlugin.Start()",1000)
                            
        },
        getListHt:function(){
            alert(viewModel.listLength());
            return listLength*(($(window).width()*0.05)+82)
        },
        isShowNoOfResultList:ko.observable(false),
        selectedGameData:ko.observable({}),
        noOfResultList:ko.observable([]),
        showNoOfResultList:function(data){
            var arr=data.type=="daily"?[{
                id:5,
                caption:"Last 5 Results"
            },{
                id:10,
                caption:"Last 10 Results"
            },{
                id:20,
                caption:"Last 20 Results"
            },{
                id:50,
                caption:"Last 50 Results"
            },{
                id:100,
                caption:"All Results ( Max 100 )"
            }]:[{
                id:5,
                caption:"Last 5 Results"
            },{
                id:10,
                caption:"Last 10 Results"
            },{
                id:20,
                caption:"Last 20 Results"
            },{
                id:50,
                caption:"Last 50 Results"
            },{
                id:100,
                caption:"All Results ( Max 100 )"
            }];
            viewModel.noOfResultList(arr);
            viewModel.resultTitle(data.name);
            viewModel.isShowNoOfResultList(true);
            viewModel.selectedGameData(data)
        },
        hideNoOfResultList:function(data){
            viewModel.isShowNoOfResultList(false)
        },
        showOverLayResults:function(count){
            viewModel.isShowNoOfResultList(false);
            var data=viewModel.selectedGameData();
            viewModel.gameType(data.type);
            viewModel.resultCount(count);
            viewModel.resultTitle(data.name);
            viewModel.resultGameId(data.id);
            viewModel.overlayVisible(true);
            viewModel.selGameDataOverLay(data);
            viewModel.setOverLayResults(data)
        },
        setOverLayResults:function(data){
            Playwin.play.setOverLayResults(data,viewModel)
        }
    };
    viewModel.dataSourceResult();
    return viewModel
};

var viewPlayGameModel={};

AppNamespace.playGame=function(params){
    var savedCardNo=window.localStorage.getItem("cardNo")?window.localStorage.getItem("cardNo"):"";
    viewPlayGameModel={
        title:ko.observable("Play Game"),
        savedCardNo:ko.observable(savedCardNo),
        isKenoGame:params.id==9,
        overlayPzSVisible:ko.observable(false),
        isShowMyAccount:ko.observable(false),
        spotOptions:ko.observableArray(Playwin.config.kenoSpots),
        amtOptions:ko.observableArray(Playwin.config.kenoAmtOpts),
        tabVisible:ko.observable(true),
        notTabVisible:ko.observable(false),
        tabClick:function(data,obj,x){
            if(data=="draws"){
                this.tabVisible(true);
                this.notTabVisible(false);
                $("#tab1").addClass("sel");
                $("#tab2").removeClass("sel")
            }else{
                if(data=="no_draws"){
                    $("#tab2").addClass("sel");
                    $("#tab1").removeClass("sel");
                    this.tabVisible(false);
                    this.notTabVisible(true)
                }
            }
        },
        showPrizeStructure:function(){
            this.overlayVisible(true)
        },
        openBannerRUrl:function(d,c){
            window.open(d,"_system")
        },
        hidePrizeStructure:function(){
            this.overlayVisible(false)
        },
        showMyAccount:function(){
            this.isShowMyAccount(true)
        },
        hideMyAccount:function(){
            this.isShowMyAccount(false)
        },
        spotChange:function(data){
            var temp=[];
            var spotBalls={};
    
            var value=$("#spot_option").val();
            if(data=="up"){
                value=parseInt($("#spot_option").val())+1;
                value=value>=(viewPlayGameModel.spotOptions().length+1)?(viewPlayGameModel.spotOptions().length+1):value
            }else{
                if(data=="down"){
                    value=parseInt($("#spot_option").val())-1;
                    value=value>=2?value:2
                }
            }
            $("#spot_option").val(value);
            $("#row_tcktprc_1").prop("checked",false);
            for(i=1;i<=value;i++){
                temp.push(i);
                spotBalls["b"+(i)]=i
            }
            Playwin.config.gameDetails.g9.ballGroups.spotVal.balls=spotBalls;
            var noBetBalls={};

            noBetBalls["b"+(i+1)]=i;
            Playwin.config.gameDetails.g9.ballGroups.noOfBet.balls=noBetBalls;
            var whls=[];
            var lp=false;
            whls=Playwin.play.getGameWheels(Playwin.play.selectedGame,lp,"spotVal");
            $("#ticket_1").scroller("destroy").scroller($.extend({
                wheels:whls
            },Playwin.play.defaultWheelPreset));
            var matchPrz=Playwin.play.getPrizeStructure($("#spot_option").val(),$("#amt_option").val());
            viewPlayGameModel.prizeStructure(matchPrz);
            viewPlayGameModel.resetGameBalls00();
            return temp
        },
        setSpotClass:function(spot){
            return"class"+spot
        },
        amtChange:function(data){
            var value=$("#amt_option").val();
            if(data=="up"){
                value=parseInt($("#amt_option").val())+1;
                value=value>=(viewPlayGameModel.amtOptions().length)?(viewPlayGameModel.amtOptions().length):value
            }else{
                if(data=="down"){
                    value=parseInt($("#amt_option").val())-1;
                    value=value>=1?value:1
                }
            }
            $("#amt_option").val(value);
            $("#amt_option_caption").val(value*10);
            if($("#spot_option").val()!=""){
                var matchPrz=Playwin.play.getPrizeStructure($("#spot_option").val(),$("#amt_option").val());
                viewPlayGameModel.prizeStructure(matchPrz)
            }
        },
        spotsArr:ko.observableArray([]),
        prizeStructure:ko.observable([]),
        firstDrawId:ko.observable(""),
        cDraw:ko.observable(""),
        cDrawName:ko.observable(""),
        nDraw:ko.observable(""),
        availableDraws:ko.observableArray([]),
        noOfDraw:ko.observableArray([{
            key:1,
            value:1
        },{
            key:2,
            value:2
        },{
            key:3,
            value:3
        },{
            key:4,
            value:4
        },{
            key:5,
            value:5
        },{
            key:6,
            value:6
        },{
            key:7,
            value:7
        }]),
        gameId:ko.observable(0),
        gameName:ko.observable(""),
        gameType:ko.observable(""),
        bannerImgUrl:ko.observable(window.localStorage.getItem("image7url")),
        bannerImgRUrl:ko.observable(window.localStorage.getItem("image7rurl")),
        gameBalls:ko.observable([]),
        cardNo:ko.observable(""),
        resultTitle:ko.observable(""),
        overlayVisible:ko.observable(false),
        overLayResults:ko.observable([]),
        resultCount:ko.observable(0),
        loadPanelVisible:ko.observable(false),
        loadPanelMsg:ko.observable("Loading ..."),
        selGameDataOverLay:ko.observable([]),
        dateBoxFr:ko.observable(new Date((new Date()).getTime()-((7+(new Date()).getDay())*24*60*60*1000))),
        dateBoxTo:ko.observable(new Date()),
        resultsHtml:ko.computed(function(){
            return"resultHtml"
        },this),
        cardInfo:ko.observable([]),
        overlayBalanceVisible:ko.observable(false),
        hideOverlayBalance:function(){
            viewPlayGameModel.overlayBalanceVisible(false)
        },
        showPopup:function(data){
            viewPlayGameModel.setOverLayResults(0,0,0,0);
            viewPlayGameModel.overlayVisible(true)
        },
        hidePopup:function(data){
            viewPlayGameModel.overlayVisible(false)
        },
        toast:{
            successMsg:"Success",
            errorMsg:"Some Error Occurred !",
            showInfo:function(){
                var toast=$("#toast-info").data("dxToast");
                toast.show()
            },
            showError:function(){
                var toast=$("#toast-error").data("dxToast");
                toast.show()
            },
            showSuccess:function(){
                var toast=$("#toast-success").data("dxToast");
                toast.show()
            },
            showWarning:function(){
                var toast=$("#toast-warning").data("dxToast");
                toast.show()
            },
            showCustom:function(){
                var toast=$("#toast-custom").data("dxToast");
                toast.show()
            }
        },
        setOverLayResults:function(){},
        resetGameBalls00:function(){
            var ginfo=eval("Playwin.config.gameDetails.g"+Playwin.play.selectedGame);
            var gameBallsTemp=[];
            $.each(ginfo.ballGroups,function(i,bg){
                if(bg.show_in_balls=="yes"){
                    $.each(bg.balls,function(n,v){
                        gameBallsTemp.push("00")
                    })
                }
            });
            viewPlayGameModel.gameBalls(gameBallsTemp)
        },
        addTicket:function(){
            if(viewPlayGameModel.checkAlertNoDraw_Draw_Amt_Spot()){
                return false
            }else{
                viewPlayGameModel.setRowLP(1,"LP");
                var checkId="";
                checkId="#row_lb_1";
                $(checkId).prop("checked",false)
            }
            viewPlayGameModel.resetGameBalls00();
            $("#ballTitle").show();
            $("#addTicket").hide()
        },
        drawChange:function(){
            var val=$("#draw_options").val();
            var arr=val.split("_",2);
            var DrawID=arr[0];
            var DrawTime=arr[1];
            var noDrawList;
            noDrawList=[];
            viewPlayGameModel.noOfDraw([]);
            $.each(viewPlayGameModel.availableDraws(),function(i,v){
                viewPlayGameModel.noOfDraw.push({
                    key:i+1,
                    value:i+1
                })
            });
            viewPlayGameModel.resetGameBalls00();
            $("#all_draws").val(DrawID);
            $("#no_draw").val("1");
            var sum=Playwin.play.calculateTotalBetAmount()
        },
        noDrawChange:function(data,obj){
            var value;
            if(data=="up"){
                value=parseInt($("#no_draw").val())+1;
                value=value>=viewPlayGameModel.noOfDraw().length?viewPlayGameModel.noOfDraw().length:value
            }else{
                if(data=="down"){
                    value=parseInt($("#no_draw").val())-1;
                    value=value>=1?value:1
                }
            }
            if($("#no_draw").val()==value){
                return
            }
            $("#selected_draw").html("");
            viewPlayGameModel.resetGameBalls00();
            $("#no_draw").val(value);
            $("#all_draws").val("");
            var sum=Playwin.play.calculateTotalBetAmount()
        },
        addNewTicketRow:function(){
            var id=$("#new_row_id").val();
            if($(".ticket").length>11){
                DevExpress.ui.dialog.alert("You can not add more then 10 tickets .","Error!");
                return false
            }
            Playwin.play.addTicketRow(Playwin.play.selectedGame,id);
            id=1+parseInt(id);
            $("#new_row_id").val(id);
            return false
        },
        kenoRowConfirm:function(){
            if($("#all_draws").val()==""&&$("#no_draw").val()==""){
                DevExpress.ui.dialog.alert("Please select a Draw OR no of draw !","Error!");
                return false
            }
            var id=$("#new_row_id").val();
            var row="<tr id='row_"+id+"'>";
            var gameGroups=Playwin.config.gameDetails.g9.ballGroups;
            var gid=Playwin.play.selectedGame;
            if($("#ticket_1").mobiscroll("getInst").getValue()==""){
                DevExpress.ui.dialog.alert("Please set ticket values .","Error!");
                return false
            }
            var values=$("#ticket_1").mobiscroll("getInst").getValue();
            var checkId="";
            checkId="#row_lb_1";
            if(!$(checkId).prop("checked")){
                var sorted_arr=values.slice(0);
                if(Playwin.play.selectedGame==2){
                    sorted_arr.pop()
                }
                sorted_arr=sorted_arr.sort();
                var flag=false;
                for(var i=0;i<sorted_arr.length-1;i++){
                    if(sorted_arr[i+1]==sorted_arr[i]){
                        DevExpress.ui.dialog.alert("Please select your nos..","Error!");
                        flag=true;
                        break
                    }
                }
                if(flag){
                    return false
                }
            }else{
                for(var i=0;i<values.length;i++){}
            }
            var vals=[];
            $("#pnlDetails").show();
            var gameDetails=eval("Playwin.config.gameDetails.g"+gid);
            var row="<tr id='row_"+id+"'>";
            row=row+"<td id='tckt_no_"+id+"' class='tckt_no'>"+($("#draws_table tr").length+1)+"</td>";
            if(gid==9){
                $.merge(vals,[$("#spot_option").val()]);
                $.merge(vals,values);
                $.merge(vals,[$("#amt_option").val()]);
                row=row+"<td >"+$("#spot_option").val()+"</td>";
                row=row+"<td ><div id='ticket_div_"+id+"' class='ticket_div'>"+($.map(values,function(n){
                    return n=="0"?"LP":n
                }))+"<input id='ticket_"+id+"' class='ticket' type='hidden' value='"+vals+"'></div></td>";
                row=row+"<td ><input id='ticket_amt_"+id+"' class='ticket_amt' type='hidden' value='"+$("#amt_option").val()+"'>"+($("#amt_option").val()*10)+"</td>"
            }else{
                row=row+"<td ><div id='ticket_div_"+id+"' class='ticket_div'>"+($.map(values,function(n){
                    return n=="0"?"LP":n
                }))+"<input id='ticket_"+id+"' class='ticket' type='hidden' value='";
                row=row+values+"'></div></td>";
                row=row+"<td ><input id='ticket_amt_"+id+"' class='ticket_amt' type='hidden' value='1'>10</td>";
                row=row+"<td ></td>"
            }
            row=row+"<td style='' width='45px' id='row_adrm_"+id+"'><input type='hidden' id='row_id_"+id+"' name='row_id[]' value='"+id+"'/>";
            row=row+"<input type='hidden' id='ticket_price_"+id+"' name='ticket_price[]' class='ticket_price' value='"+gameDetails.ticketPrice+"'/>";
            row=row+"<div onClick='Playwin.play.removeTicket("+id+")' style='cursor:pointer'  id='row_rm_"+id+"' class='row_remove' title='Remove Draw'> <img src='content/images/trash.png' class='deleteIcon'/> </div></td></tr>";
            $("#draws_table").append(row);
            var sum=Playwin.play.calculateTotalBetAmount();
            id=1+parseInt(id);
            $("#new_row_id").val(id);
            $("#ballTitle").hide();
            $("#addTicket").show()
        },
        checkAlertNoDraw_Draw_Amt_Spot:function(){
            var flag=false;
            if($("#no_draw").val()==""&&$("#all_draws").val()==""){
                DevExpress.ui.dialog.alert("Please select a No of draw OR Advanced draw .","Error!");
                flag=true
            }
            if(Playwin.play.selectedGame==9){
                if($("#spot_option").val()==""||$("#amt_option").val()==""){
                    DevExpress.ui.dialog.alert("Please select a spot & amount to bet .","Error!");
                    flag=true
                }
            }
            return flag
        },
        setRowLP:function(id,lpBtn){
            if(viewPlayGameModel.checkAlertNoDraw_Draw_Amt_Spot()){
                return false
            }
            var prevVal=$("#ticket_"+id).mobiscroll("getInst")?"":$("#ticket_"+id).mobiscroll("getInst").getValue();
            var grpNm="all";
            var lp=false;
            var checkId="";
            checkId="#row_lb_"+id;
            if(lpBtn=="LP"){
                $(checkId).prop("checked",true);
                $("#ticket_1_prev").val(1)
            }else{
                $(checkId).prop("checked",false)
            }
            lp=$(checkId).prop("checked");
            var whls=[];
            whls=Playwin.play.getGameWheels(Playwin.play.selectedGame,lp,grpNm);
            $("#ticket_"+id).scroller("destroy").scroller($.extend({
                wheels:whls
            },Playwin.play.defaultWheelPreset));
            var gameBalls=[];
            if(lpBtn=="LP"){
                var gameBalls1=[];
                $.each(viewPlayGameModel.gameBalls(),function(i,v){
                    gameBalls.push(0);
                    gameBalls1.push("LP")
                });
                viewPlayGameModel.gameBalls(gameBalls1)
            }else{
                gameBalls=prevVal;
                $("#ticket_"+id).mobiscroll("show");
                viewPlayGameModel.resetGameBalls00()
            }
            $("#ticket_"+id).mobiscroll("setValue",gameBalls,true,1,false);
            return false
        },
        showScroller:function(){
            $("#ticket_"+1).mobiscroll("show")
        },
        play:function(){
            Playwin.play.go(Playwin.play.selectedGame)
        },
        isShowNoOfResultList:ko.observable(false),
        selectedGameData:ko.observable({}),
        noOfResultList:ko.observable([]),
        showNoOfResultList:function(data){
            var arr=viewPlayGameModel.gameType()=="daily"?[{
                id:5,
                caption:"Last 5 Results"
            },{
                id:10,
                caption:"Last 10 Results"
            },{
                id:20,
                caption:"Last 20 Results"
            },{
                id:50,
                caption:"Last 50 Results"
            },{
                id:100,
                caption:"All Results ( Max 100 ) "
            }]:[{
                id:5,
                caption:"Last 5 Results"
            },{
                id:10,
                caption:"Last 10 Results"
            },{
                id:20,
                caption:"Last 20 Results"
            },{
                id:50,
                caption:"Last 50 Results"
            },{
                id:100,
                caption:"All Results ( Max 100 ) "
            }];

            var data = {};
            data.gameType = viewPlayGameModel.gameType();
            data.id = viewPlayGameModel.gameId();
            data.name = viewPlayGameModel.gameName();
            viewPlayGameModel.resultTitle(data.name);
            viewPlayGameModel.noOfResultList(arr);
            viewPlayGameModel.isShowNoOfResultList(true);
            viewPlayGameModel.selectedGameData(data)
        },
        hideNoOfResultList:function(data){
            viewPlayGameModel.isShowNoOfResultList(false)
        },
        showOverLayResults:function(count){
            var data={
                id:viewPlayGameModel.gameId(),
                name:viewPlayGameModel.gameName()
            };
         
    
            viewPlayGameModel.isShowNoOfResultList(false);
            viewPlayGameModel.resultCount(count);
            viewPlayGameModel.resultTitle(data.name);
            viewPlayGameModel.selGameDataOverLay(data);
            viewPlayGameModel.setOverLayResults(data);
            viewPlayGameModel.overlayVisible(true)
        },
        setOverLayResults:function(data){
            Playwin.play.setOverLayResults(data,viewPlayGameModel)
        },
        showOverlayBalance:function(){
            var no=$("#cardNo").val();
            var pass=$("#pinNo").val();
            if(!no||$.trim(no)==""){
                DevExpress.ui.dialog.alert("Card Number can not be empty !","Error");
                return false
            }else{
                if(!pass||$.trim(pass)==""){
                    DevExpress.ui.dialog.alert("Card Pin can not be empty .","Error");
                    return false
                }
            }
            viewPlayGameModel.loadPanelMsg("Loading...");
            viewPlayGameModel.loadPanelVisible(true);
            $.ajax({
                url:Playwin.config.urls.checkBalance,
                method:"GET",
                data:"accountno="+$.trim(no)+"&password="+$.trim(pass),
                success:function(xmlData){
                    var jsonObj=$.xml2json(xmlData);
                    viewPlayGameModel.loadPanelVisible(false);
                    viewPlayGameModel.loadPanelMsg("Loading ...");
                    if(jsonObj.Error||(jsonObj.Code&&(jsonObj.Code==-1||jsonObj.Code==99))){
                        DevExpress.ui.dialog.alert(jsonObj.Error,"Invalid Card Error")
                    }else{
                        if(!jsonObj.RemainingAmount){
                            DevExpress.ui.dialog.alert("Some error occurred when getting card balance. ","Card Read Error!")
                        }else{
                            viewPlayGameModel.cardInfo(jsonObj);
                            viewPlayGameModel.overlayBalanceVisible(true)
                        }
                    }
                }
            }).fail(function(){
                viewPlayGameModel.loadPanelVisible(false);
                viewPlayGameModel.loadPanelMsg("Loading ...");
                DevExpress.ui.dialog.alert("Error : Unable get card info.","Error!")
            })
        },
        tbItems:ko.observable([{
            align:"left",
            widget:"button",
            options:{
                icon:"back",
                clickAction:"#_back"
            }
        },{
            text:"Play Game",
            align:"left"
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"resultTop",
                clickAction:this.showOverLayResults
            }
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"home",
                clickAction:"#Home"
            }
        }])
    };

    viewPlayGameModel.loadPanelMsg("Loading...");
    viewPlayGameModel.loadPanelVisible(true);
    $.get(Playwin.config.urls.gameDetails).done(function(xmlData){
        viewPlayGameModel.loadPanelVisible(false);
        var jsonObj=$.xml2json(xmlData);
        $.each(Playwin.config.gameList,function(i,val){
            var g;
            g="Playwin.config.gameDetails.g"+val.id;
            var gid;
            gid="Playwin.config.gameDetails.g"+val.id+".draws";
            if(eval(g)&&eval(gid)){
                eval(gid).splice(0,eval(gid).length)
            }
        });
        $.each(jsonObj.Draw,function(i,val){
            var g;
            g="Playwin.config.gameDetails.g"+val.GameID;
            var gid;
            gid="Playwin.config.gameDetails.g"+val.GameID+".draws";
            if(eval(g)&&eval(gid)){
                var temp={};
        
                eval(gid).push(val)
            }
        });
        var ginfo=eval("Playwin.config.gameDetails.g"+params.id);
        viewPlayGameModel.firstDrawId(ginfo.draws[0].DrawID);
        viewPlayGameModel.cDraw(ginfo.name+"( "+ginfo.amt+" )");
        viewPlayGameModel.cDrawName(ginfo.name);
        viewPlayGameModel.gameName(ginfo.name);
        viewPlayGameModel.gameType(ginfo.type);
        viewPlayGameModel.resultCount(ginfo.type=="daily"?5:3);
        viewPlayGameModel.gameId(ginfo.id);
        viewPlayGameModel.title(ginfo.name);
        viewPlayGameModel.tbItems([{
            align:"left",
            widget:"button",
            options:{
                icon:"back",
                clickAction:"#_back"
            }
        },{
            text:"Play  "+ginfo.name,
            align:"left"
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"resultTop",
                clickAction:viewPlayGameModel.showNoOfResultList
            }
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"home",
                clickAction:"#Home"
            }
        }]);
        viewPlayGameModel.nDraw(Playwin.core.getFormatedDateTime(ginfo.draws[0].DrawTime,"dddd, dd-MMM-yyyy hh:mm TT"));
        viewPlayGameModel.availableDraws(ginfo.draws);
        viewPlayGameModel.noOfDraw([]);
        $.each(viewPlayGameModel.availableDraws(),function(i,v){
            viewPlayGameModel.noOfDraw.push({
                key:i+1,
                value:i+1
            })
        });
        var whls=[[{
            label:"",
            keys:[],
            values:[]
        }]];
        $.each(ginfo.draws,function(i,v){
            whls[0][0].keys.push(v.DrawID);
            whls[0][0].values.push(v.DrawTime)
        });
        var gameBalls=[];
        $.each(ginfo.ballGroups,function(i,bg){
            $.each(bg.balls,function(n,v){
                gameBalls.push("00")
            })
        });
        viewPlayGameModel.gameBalls(gameBalls);
        Playwin.play.jsonDetails.currJackpot.id=Playwin.play.selectedGame;
        Playwin.play.jsonDetails.currJackpot.name=ginfo.name;
        Playwin.play.jsonDetails.currJackpot.amt=ginfo.amt;
        Playwin.play.jsonDetails.currJackpot.displayName=ginfo.name+"( "+ginfo.amt+" )";
        Playwin.play.jsonDetails.nextDraw=Playwin.core.getFormatedDateTime(ginfo.draws[0].DrawTime,"dddd, dd-MMM-yyyy hh:mm TT");
        Playwin.play.loadData(params.id)
    }).fail(function(){
        viewPlayGameModel.loadPanelVisible(false);
        DevExpress.ui.dialog.alert("Error : Game details load Error .","Error!")
    });
    $(".textBox").click(function(){
        this.focus();
        return false
    });
    return viewPlayGameModel
};

AppNamespace.playGameSuccess=function(c){
    viewPlayGameModel=null;
    var d={
        gameId:c.id,
        bannerImgUrl:ko.observable(window.localStorage.getItem("image5url")),
        bannerImgRUrl:ko.observable(window.localStorage.getItem("image5rurl")),
        tbItems:[{
            align:"left",
            template:"nav-button"
        },{
            text:"Congratulation !!!",
            align:"center"
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"home",
                clickAction:"#Home"
            }
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"user",
                clickAction:"#Home"
            }
        }],
        playGameBtn:function(){
            AppNamespace.app.clearState();
            AppNamespace.app.navigate("playGame/"+this.gameId)
        },
        openBannerRUrl:function(a,b){
            window.open(a,"_system")
        },
        tickets:Playwin.play.playGameResponse
    };

    return d
};

AppNamespace.aboutUs=function(){
    var b={
        message:ko.observable("About Us"),
        tbItems:[{
            align:"left",
            template:"nav-button"
        },{
            text:"About Playwin",
            align:"center"
        },{
            align:"right",
            widget:"button",
            options:{
                icon:"home",
                clickAction:"#Home"
            }
        }],
        items:[{
            key:"",
            items:[{
                name:"About Us",
                url:"about"
            },{
                name:"Our Values",
                url:"ourValues"
            },{
                name:"Winners Club",
                url:"winnersYearList"
            }]
        }],
        callAction:function(a){
            AppNamespace.app.navigate(a.url)
        }
    };

    return b
};

//{'name':'back','showIcon':false,'align':'left'},